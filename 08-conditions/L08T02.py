
'''
Tee ohjelma joka kysyy käyttäjältä 3 kokonaislukua ja tulostaa niistä suurimman.
'''


luku1 = int(input("Anna kokonaisluku #1: "))
luku2 = int(input("Anna kokonaisluku #2: "))
luku3 = int(input("Anna kokonaisluku #3: "))


if luku1 >= luku2 and luku1 >= luku3:
    print("suurin numero on #1, arvolla = ",luku1)
elif luku1 <= luku2 and luku2 >= luku3:
    print("suurin numero on #2, arvolla =  ",luku2)
else:
    print("suurin numero on #3, arvolla =  ",luku3)


