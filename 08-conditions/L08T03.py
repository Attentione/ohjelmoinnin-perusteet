'''
Tee ohjelma joka kysyy käyttäjältä kokonaisluvun.

Jos luku on 10 tai 20, tulosta teksti "Number is 10 or 20".
Jos luku on 100 tai 200, tulosta teksti "Number is 100 or 200".
Muuten tulosta numero konsoliin.
'''

luku = int(input("Anna kokonaisluku: "))

if luku == 10 or luku == 20:
    print("Number is 10 or 20")

elif luku == 100 or luku == 200:
    print("Number is 100 or 200")

else:
    print("number is: ",luku)