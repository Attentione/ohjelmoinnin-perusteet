'''
Tee ohjelma, joka kysyy käyttäjältä viisi lukua. Laske annetuista luvuista yhteen ne luvut, jotka ovat nollaa suurempia.
Eli jos käyttäjä antaa nollan tai negatiivisen luvun sitä ei lisätä summaan. Tulosta summa konsoliin. Kokeile ohjelmaasi esim seuraavilla arvoilla: 1,2,3,4,5 ja -2,0,2,4,6. 
Mitä sait summaksi?
'''


luku1 = int(input("Anna kokonaisluku #1 :"))
luku2 = int(input("Anna kokonaisluku #2 :"))
luku3 = int(input("Anna kokonaisluku #3 :"))
luku4 = int(input("Anna kokonaisluku #4 :"))
luku5 = int(input("Anna kokonaisluku #5 :"))


if luku1 > 0:
    test1 = luku1
else:
        test1 = 0
if luku2 > 0:
    test2 = luku2
else:
        test2 = 0
if luku3 > 0:
    test3 = luku3
else:
        test3 = 0
if luku4 > 0:
    test4 = luku4
else:
        test4 = 0
if luku5 > 0:
    test5 = luku5
else:
        test5 = 0


summa = test1 + test2 + test3 + test4 + test5

print(luku1, luku2, luku3, luku4, luku5)
print("Lukujen summa =", summa)