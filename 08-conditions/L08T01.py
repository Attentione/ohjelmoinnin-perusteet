'''
Tee ohjelma jossa kysytään käyttäjältä tämän ikä.

jos ikä on alle 13 vuotta, tulosta "child"
jos ikä on 13-19 vuotta, tulosta "teen"
jos se on 20-65 vuotta, tulosta "adult"
muussa tapauksessa tulosta "senior".
'''


ika = int(input("Kuinka vanha olet? "))


if ika < 13:
    print("child")

elif ika >= 13 and ika <= 19:
    print("teen")

elif ika >= 20 and ika <=65:
    print("adult")

else:
    print("senior")
    
