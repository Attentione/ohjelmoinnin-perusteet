'''
pisteraja   arvosana
0-1             0
2-3             1
4-5             2
6-7             3
8-9             4
10-12           5

Tee ohjelma jossa annetaan oppilaalle arvosana taulukon mukaan.
Kysy pistemäärä konsolissa tulosta arvosana.
'''

pisteet = int(input("Montako pistettä annetaan?: "))

if pisteet <= 1:
    print("Arvosana = 0")
elif pisteet == 2 or pisteet == 3:
    print("Arvosana = 1")
elif pisteet == 4 or pisteet == 5:
    print("Arvosana = 2")
elif pisteet == 6 or pisteet == 7:
    print("Arvosana = 3")
elif pisteet == 8 or pisteet == 9:
    print("Arvosana = 4")
elif pisteet >= 10 or pisteet <= 12:
    print("Arvosana = 5")



    
