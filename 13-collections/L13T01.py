'''
Tehtävä A
Toteuta ohjelma, joka kysyy käyttäjältä autojen rekisterinumeroita (siis esim 'ABC-123' jne) ja tallentaa ne listaan. 
Käyttäjä voi syöttää niin monta rekisterinumeroa kuin haluaa, syöttäminen lopetetaan tyhjällä syötteellä. Näytä syötetyt rekisterinumero aakkosjärjestyksessä.
'''


def rekkarit():
    lista = []
    while True:
        rekkari = input("Tyhjä syöte lopettaa ohjelman, Anna rekisterinumero: ")
        aakkos_lista = sorted(lista)
        
        if  rekkari != "":
            lista.append(rekkari)
            continue
     
        else:
            print(aakkos_lista)
            break
            

rekkarit()
