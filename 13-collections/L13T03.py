'''
Toteuta ohjelma, johon voi tallentaa kymmenen eri auton tiedot. Kustakin autosta tiedetään rekisterinumero (esim ABC-123) ja autonmerkki (esim Skoda). 
Keksi itse erilaisia rekisterinumeroita ja automerkkejä. Tallenna tiedot valitsemaasi kokoelmaan. 
Tulosta sen jälkeen autojen tiedot ensin aakkosjärjestyksssä automerkin mukaan, ja sen jälkeen aakkosjärjestyksessä rekisterinumeron mukaan.
'''
import random
import string

merkit = ['Audi', 'BMW', 'Ford', 'Opel', 'Skoda', 'Volvo', 'VW']

def register():
    rekisteri = {}
    for i in range(10):
        letters = string.ascii_lowercase
        alku = ''.join(random.choice(letters) for i in range(3))
        loppu = str(random.randrange(100, 999))
        rekkari = alku + "-" + loppu
        merkki = random.choice(merkit)
        rekisteri[rekkari] = merkki
    return rekisteri

rekisteri = register()


def sort_key():  

    keys = sorted(rekisteri.keys(), key=lambda x:x.lower())

    sorted_dict= {}
    for key in keys:
        sorted_dict.update({key: rekisteri[key]})

    print(sorted_dict)


value = sorted(rekisteri.items(), key=lambda x: x[1])    

sort_key()
print(value)
