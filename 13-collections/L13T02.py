'''
Tehtävä A
Tee ohjelma joka kysyy käyttäjältä kurssien arvosanoja (arvosana on kokonaisuluku 0,1,2,3,4 tai 5) ja tallentaa ne listaan. 
Käyttäjä voi syöttää niin monta kurssiarvosanaa kuin haluaa, syöttäminen lopetetaan tyhjällä syötteellä. Näytä lopuksi montako arvosanaa käyttäjä antoi ja arvosanojen keskiarvo.
'''

def arvosanat():
    summa = 0
    lista = []
    while True:
        numerot = input("Tyhjä lopettaa ohjelman. Anna arvosana 0 - 5: ")
        
        
        if  numerot == "":
            break
     
        else:
            number = int(numerot)
            lista.append(number)
            summa += 1
            avg = (sum(lista)/summa)
            continue
    print(f"Annoit {summa} arvosanaa, arvosanojen keskiarvo = {avg}")
            
    
arvosanat()