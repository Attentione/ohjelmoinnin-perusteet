'''
Tee seuraava lisäys edelliseen tehtävään. Luo satunnaisesti vähintään viisi erilaista auto-oliota seuraavista automerkeistä 
(brand) 'Audi', 'BMW', 'Ford', 'Opel', 'Skoda', 'Volvo' ja 'VW'. Autojen model ominaisuuden voit jättää halutessasi tyhjäksi. 
Generoi satunnaisesti niille hinta väliltä 1000-10000. Lisää ne sopivaan kokoelmaan, siis esim listaan tai dictionaryyn tms.
Tulosta kaikkien auton ominaisuudet konsolille.
Laske myös autojen yhteishinta, ja näytä se konsolilla.
'''
import random

class Car:
    brand = ""
    price = 0
    
    def __init__(self, brand, price):
        self.brand = brand
        self.price = price

    def __str__(self):
        return "Brand = " + self.brand + " , Price = " + str(self.price)

merkit = ['Audi', 'BMW', 'Ford', 'Opel', 'Skoda', 'Volvo', 'VW']
hinta = random.randrange(1000, 10000)
brand_list = []
total = []

for i in range(5):
    random_merkki = random.choice(merkit)
    brand_list.append(random_merkki)
    hinta = random.randrange(1000, 10000)
    brand_list.append(hinta)
    total.append(hinta)
price = sum(total)


car1 = Car(brand_list[0], brand_list[1])
car2 = Car(brand_list[2], brand_list[3])
car3 = Car(brand_list[4], brand_list[5])
car4 = Car(brand_list[6], brand_list[7])
car5 = Car(brand_list[8], brand_list[9])


print(car1)
print(car2)
print(car3)
print(car4)
print(car5)
print("Autojen yhteenlaskettu arvo = ", price)

