'''
Tee luokka Human. Luokalla on kaksi ominaisuutta name ja age. 
Luo Human-luokasta kaksi olioita, joitten arvot asetat. Tulosta olioiden tiedot konsolille
'''



class Human:
    name = ""
    age = 0

    def show_info(self):
        output = "Nimi = " + self.name + " ,Ikä = " + str(self.age)
        return output


human1 = Human()
human1.name = "Atte"
human1.age = 35

human2 = Human()
human2.name = "Seppo"
human2.age = 50



print(human1.show_info())
print(human2.show_info())
