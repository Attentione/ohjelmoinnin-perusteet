'''
Tee luokka Cat. Tee luokalla on kaksi ominaisuutta name ja color, sekä yksi metodi miau. 
Luo Cat-luokasta vähintään kaksi erilaista kissa-oliota. Näytä kissa-olioiden ominaisuudet konsolille, laita kissat myös naukumaan.
'''


class Cat:
    name = ""
    color = ""
    

    
    def show_info(self):
        output = "Nimi = " + self.name + " ,Väri = " + self.color
        return output

    def mau():
        return "Kissa sanoo GURR MAU"



kisu1 = Cat()
kisu1.name = "Olavi"
kisu1.color = "Grey and white"

kisu2 = Cat()
kisu2.name = "Elli"
kisu2.color = "Black and white"




print(kisu1.show_info()," , ",Cat.mau())
print(kisu2.show_info()," , ",Cat.mau())