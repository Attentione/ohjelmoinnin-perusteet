# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Kehitysympäristö**
Tällä kurssilla käytetään [Python](https://www.python.org/) ohjelmointikieltä. Python on [Guido van Rossumin](https://en.wikipedia.org/wiki/Guido_van_Rossum) kehittämä, vuonna 1991 julkaistu ohjelmointikieli. Pythonin suunnittelufilosofia keskittyi koodin luettavuuteen. Sen kielirakenne ja olio-ohjelmointipohjainen lähestymiskulma pyrkii auttamaan ohjelmoijia kirjoittamaan selkeää ja loogista lähdekoodia niin pienissä kuin suuremmissakin ohjelmistoprojekteissa.


## **Python**
Python on nykyään erittäin suosittu ohjelmointikieli kaikenlaisessa kehitystyössä. Sillä voi ohjelmoida periaatteessa mitä tahansa komentoriviohjelmista aina peleihin ja täysiin verkkopalveluihin.

Python on moniparadigmainen ohjelmointikieli. Oliokeskeistä ohjelmointia ja strukturoitua ohjelmointia tuetaan täysin, ja monet sen ominaisuuksista tukevat funktionaalista ohjelmointia. Monia muita paradigmoja tuetaan laajennusten kautta.

Python käyttää dynaamista tyypitystä ja automaattista roskien kerääjää muistin hallintaan. Siinä on myös dynaaminen nimien resoluutio (myöhäinen sidonta), joka sitoo funktion ja muuttujien nimet ohjelman suorituksen aikana.

Sen sijaan että kaikki toiminnallisuus olisi sisällytetty kielen ytimeen, Python suunniteltiin erittäin laajennettavaksi. Tämä kompakti modulaarisuus on tehnyt siitä erityisen suositun välineen lisätä ohjelmoitavia rajapintoja olemassa oleviin sovelluksiin.

Python-kehittäjien tärkeä tavoite on että kielen käyttämisen tulee olla hauskaa. Tämä heijastuu kielen nimessä - kunnianosoitus brittiläiselle komediaryhmälle Monty Pythonille - ja toisinaan leikkisissä lähestymistavoissa opetusohjelmiin ja viitemateriaaleihin, kuten esimerkkeihin, jotka viittaavat purkkilihaan ja muniin (spam and eggs, kuuluisasta Monty Python -sketsistä) tyypillisemmän foo ja bar sijaan.

Python-yhteisössä yleinen neologismi on ***pythonic***, jolla voi olla laaja valikoima ohjelmointityyliin liittyviä merkityksiä. Sanoa, että koodi on ***pythoninen***, tarkoittaa sitä, että se käyttää Python-idioomeja hyvin, että se on luonnollista tai osoittaa kielen sujuvuutta, että se on Pythonin minimalistisen filosofian ja luettavuuden painottamisen mukainen. Sitä vastoin koodia, jota on vaikea ymmärtää kutsutaan olevan ***unpythonic***.


## **Miksi Python?**
Python on monipuolinen, tulkattava ohjelmointikieli ja sopii erityisesti verkkosovelluksiin. Pythonia voi käyttää myös vaativassa ja tieteellisessä laskennassa ja sitä käyttäen voi tuottaa myös graafisia esityksiä. Pythonia pidetään helppona oppia sen yksinkertaisen syntaksin(lauseopin) ja korkean tason tietorakenteiden takia. Monet suosittelevat sitä ensimmäiseksi ohjelmointikieleksi.


## **Visual Studio Code**
Visual Studio Code on Microsoftin kehittämä avoimen lähdekoodin ohjelmointiympäristö. Sen ominaisuuksiin kuuluu tuki debuggaugselle, syntaksikorostaminen, älykäs koodin loppuun saattaminen ja upotettu tuki Git- versionhallinnalle. Käyttäjät voivat muuttaa teemaa, pikanäppäimiä, asetuksia ja asentaa lisäominaisuuksia lisääviä laajennuksia.

Tällä hetkellä (2020) Visual Studio Code on erittäin suosittu kehitysympäristö. Vuonna 2016 [Stack Overflow](https://stackoverflow.com) tekemän tutkimuksen mukaan Visual Studio Code oli 13. suosituin kehitysympäristö ja vain 7.2% 46614:sta vastaajasta kertoi käyttävänsä sitä. Jo kaksi vuotta myöhemmin Visual Studio Code oli noussut ykköspaikalle ja 34.9% 75398:sta vastaajasta kertoi käyttävänsä VS Codea. Viimeisimmän kyselyn (2019) mukaan jo 50.7% 87317:sta vastaajasta kertoi käyttävänsä VS Codea.

VS Code tukee Pythonia erillisen laajennuksen kautta. Toki VS Codella voi kirjoittaa Python lähdekoodia myös ilman laajennuksen asentamista. Silloin kirjoitettu lähdekoodi on suoritettava erikseen komentotulkissa.

VS Code on ilmainen ja saatavilla Windows ja Linux tietokoneille. Voit ladata sen tietokoneellesi [Microsoftin sivuilta](https://code.visualstudio.com).


## **Kehitysympäristön asentaminen**
1. Lataa ja asenna [Python](https://www.python.org/downloads/)
    - Näissä materiaaleissa käytetään Python versiota 3.8.5
    - Voit halutessasi käyttää asennusohjelman 'Remove the MAX_PATH Limitation' toimintoa, mutta se ei ole tarpeen mikäli tietokoneessasi ei ole useita kehitysympäristöjä asennettuna.
    - Asennuksen jälkeen varmistetaan että Python ympäristö on käytettävissä:
        - Avaa Windowsin komentotulkki (Command Prompt)
            - Windows+Q ja kirjoita ***cmd*** tai Windows+R ja kirjoita ***cmd***
        - Kirjoita komentotulkkiin komento ***py*** ja varmista että Python tulkki käynnistyy
        - Voit poistua Python tulkista näppäinyhdistelmällä Ctrl+Z

Python tulkki käynnistetty Windowsin komentotulkissa:

![](./img/python_cmd_installed.png "Python asennettu ja toimii komentotulkissa")

2. Lataa ja asenna [Git- versionhallinta](https://git-scm.com/downloads)
    - Asennusohjelman oletusasetukset toimivat hyvin eikä niitä tarvitse muuttaa
3. Lataa ja asenna [Visual Studio Code](https://code.visualstudio.com)
4. Asenna VS Code Python laajennus
    - Avaa VS Code ja paina 'Extensions' nappia
        - ![](./img/vs_code_extensions_button.png "Extensions nappi")
    - Laita hakusanaksi Python ja asenna Microsoftin kehittämä Python laajennus
    - Asennus aloitetaan pienellä vihreällä ***install*** napilla:
        - ![](./img/vs_code_python_extension.png "Python laajennuksen asennus")
    - Asennuksen jälkeen VS Code ilmoittaa että Python tulkkia ei ole valittu
        - Paina 'Select Python Interpreter' nappia:
            - ![](./img/vs_code_python_interpreter.png "Python tulkin valinta")
            - Mikäli yllä oleva valintalaatikko ei ilmesty, voit asettaa Python tulkin myös klikkaamalla VS Coden alapalkissa vasemmalla olevaa valintalaatikkoa.
    - Edit laatikko VS Coden yläreunassa aktivoituu, valitse sieltä Python 3.8.5 tulkki


## **Kehitysympäristö, yhteenveto**

Nyt sinulla on asennettuna kaikki tarpeellinen tämän kurssin materiaalien seuraamista varten. Seuraavaksi teemme ensimmäisen Python-ohjelman.


&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

