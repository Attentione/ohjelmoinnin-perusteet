# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Kurssiesittely**
Tavoitteena on oppia ohjelmoinnin perusteet sekä git-versionhallinan perusteet.
Työkaluina käytämme Microsoftin Visual Studio Code kehitysympäristöä sekä Python ohjelmointikieltä.

Kurssitunnus: TTC2030
Laajuus: 5 opintopistettä


## **Edeltävä osaaminen**
Kurssin sujuvaan suorittamiseen tarvitset seuraavia taitoja:
- Windows tietokoneen peruskäyttö
- Levyasemien, kansioiden ja tiedostojen hallinta
- Sujuva tekstin muokkaaminen, leikepöydän tehokas käyttö


## **Materiaalit ja läsnäolo**
- Kaikki kurssin materiaalit löytyvät JAMK labraverkon GitLab palvelusta
    - Tarvitset JAMK labranet tunnukset että pääset käsiksi materiaaleihin.
    - Kurssi on mahdollista suorittaa myös käyttämällä julkista GitLab palvelua, tällöin jotkut kurssilla annetut ohjeet saattavat poiketa hieman.
- Kurssimateriaalit ovat suomeksi, poislukien lähdekoodi joka on aina kirjoitettu englanniksi.
    - Suositus on että kaikki ohjelmointitehtävät tulee olla ohjelmoitu englannin kielellä. 
- Jos työskentelet JAMK labranetin ulkopuolella, asenna
    - Python
    - Visual Studio Code
    - Git
- Kurssilla ei ole läsnäolopakkoa


## **Tehtävät**
1. Harjoitustehtävät
    - Kurssiin liittyy 10 tehtäväpakettia
2. Harjoitustyö
    - Opiskelija ehdottaa harjoitustyön aiheen opettajalle
        - lähetä aihe-ehdotuksesti opettajalle teams-viestillä, sähköpostilla tai esittele ehdotuksesti luennoilla.
        - voit ehdottaa aihetta heti kun tiedät mitä haluat tehdä
    - Opettaja hyväksyy aiheen
3. Itsearviointi
4. Koe (monivalintatehtäviä)


## **Harjoitustyö**
- Harjoitustyö kannattaa pitää melko pienenä
    - Toki monimutkaisesta harjoitustyöstä voi saada lisäpisteitä (jos se toimii), mutta pieni ja napakka ohjelma riittää mainiosti täysien pisteiden saamiseen.
- Harjoitustyön ei tarvitse olla kunnianhimoinen, eikä sen tarvitse ratkaista mitään tosielämän ongelmaa.
- Tärkeintä on että lähdekoodissa käytetään kattavasti kurssilla opittuja ominaisuuksia ja koodi on siistiä ja johdonmukaista
- Harjoitustyön palauttamisen jälkeen täytä itsearviointi kurssin Moodle sivulla.


## **Arviointi**
Kurssi arvioidaan pisteytyksellä
- Harjoitustehtävät, max 100 pistettä (läpäisyraja 10 pistettä)
    - Kaikki palautetut harjoitukset tarkastetaan hyväksytty/hylätty periaatteella, eli 10 pisteen tehtävästä voit saada 0 tai 10 pistettä.
- Koe, max 50 pistettä (läpäisyraja 10 pistettä)
- Harjoitustyö, max 50 pistettä (läpäisyraja 10 pistettä)
- YHT: max 200 pistettä

Jos osaat jo kaiken, sovi opettajan kanssa vaihtoehtoinen suoritustapa


## **Arvosana**
Arvosana määräytyy pisteiden perusteella seuraavasti.

| Pisteet   | Arvosana  |
| :-------- | --------: |
| 0-29      | 0         |
| 30-79     | 1         |
| 80-109    | 2         |
| 110-139   | 3         |
| 140-169   | 4         |
| 170-200   | 5         |


&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

