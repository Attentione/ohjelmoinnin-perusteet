# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Koodin kirjoittaminen**
Tutustutaan erilaisiin tapoihin, sääntöihin ja muihin perusasioihin jotka liittyvät lähdekoodin kirjoittamiseen.

- [Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md): **/examples/06-code/code.py**


## **Varatut sanat**
Jokaisessa ohjelmointikielessä on varattuja sanoja joita ei voi käyttää muihin tarkoituksiin. Python kielen varatut sanat käsittää reilun 30 sanan listan.

Listalta löytyvät mm:
- Ehtolauseisiin käytettävät sanat (if, else, and, or, not)
- Toistolauseiden käsittelyt (for, while, from)

VS Code näyttää kielen varatut sanat purppuralla, funktiokutsut keltaisella, ja kommentit vihreällä värillä, joten ne on helppo havaita tekstieditorissa.

Huomaa että eri koodieditorit käyttävät eri värejä ja usein väritystä voi myös muuttaa itselle mieluisaksi.


## **Tyyliohjeet**
Yksi ohjelmoijan tärkeimmistä tehtävistä on kirjoittaa lähdekoodi annettun tyyliohjeen mukaisesti. Tietokone ei välitä miten koodi on kirjoitettu kunhan se seuraa ohjelmointikielen kielioppia, mutta tiimityöskentelyn kannalta on tärkeää että jokainen tiimin jäsen pystyy lukemaan ja ymmärtämään koodia sujuvasti.

Tyyliohjeita on useita ja usein yrityksillä on omat hieman muista poikkeavat tyyliohjeet. Onkin tärkeää oppia soveltamaan erilaisia nimeämiskäytäntöjä eikä jämähtää yhteen.

[Python.org](https://python.org):lla on oma [tyyliohje](https://www.python.org/dev/peps/pep-0008/)

Esimerkiksi Googlella on oma nimeämiskäytäntö/tyyliohje [Googlen Python tyyliohje](https://google.github.io/styleguide/pyguide.html)

Ohjelmoija voi itse määrittää muuttujien, funktioiden ja niiden parametrien, luokkien, sekä luokkien jäsenten nimet. Ehtona on ettei niminä voi käyttää mitään ohjelmointikielen varattuja sanoja.

Python kieli on kirjainkoko-sensitiivi, eli muuttujat number_a ja number_A ovat eri muuttujia. Tällaista nimeämistä tulee kuitenkin välttää koska se on tarpeetonta ja tekee koodista vaikealukuista.

Me käytämme tällä kurssilla python.org mukaista tyyliohjetta. Lyhyesti:
- funktioiden ja muuttujien nimet kirjoitetaan kaikki pienillä kirjaimilla ja eri sanat erotellaan alaviivalla
    - ***function***, ***my_function***, ***variable***, ***my_variable***
- Luokkien nimet alkavat isolla alkukirjaimella ja eri sanat aloitetaan uudella isolla kirjaimella
    - ***Vehicle***, ***MyClass***, ***MovieDatabase***


# **Lähdekoodin kommentointi**
Yhtä tärkeä osa kuin aikaisemmin nimeämiskäytännöt on säännöt lähdekoodin kommentoimisesta. Koodia kommentoidaan samasta syystä kun nimeämiskäytäntöjä seurataan; halutaan kirjoittaa luettavampaa koodia.

Python kielessä on käytössä kahdenlaisia kommentteja.
- Rivikommentti
    - Rivi alkaa 'ritilä' # merkillä
    - Kaikki teksti siitä alkaen seuraavalle riville asti on kommenttia
- Kommenttiblokki
    - blokki alkaa ja päättyy kolmella lainausmerkillä """
    - Kaikki teksti näiden välissä on kommenttia

Koodia kannattaa kommentoida järkevästi. Itsestäänselvyyksiä ei tarvitse/pidä kommentoida, mutta jos joku algoritmi ei ole itsestään selvä niin on hyvä kommentoida se.

Voit myös kysyä itseltäsi: "Ymmärränkö mitä tämä tekee kun palaan katsomaan koodia 3 kuukauden päästä". Mikäli et ole varma, koodi luultavasti ansaitsee tulla kommentoiduksi.

Kommentteja yleensä kirjoitetaan seuraavasti:
- Lähdekooditiedoston alkuun kommenttiblokki joka kertoo tiedoston tarkoituksen, tekijän, päivämäärän, tekijänoikeudet, lisenssin yms.
- Funktioiden alkuun kertomaan funktion tarkoituksesta, parametreista ja paluuarvosta
- Funktioiden sisään kun siellä olevan algoritmin toiminta ei ole itsestäänselvää.

Funktioiden ja niiden parameterien kommentoimiseen suositellaan [Docstring](https://www.python.org/dev/peps/pep-0257/) menetelmää jossa funktio kommentoidaan sen koodin alkuun.

Kirjota tämä koodi alussa luomaasi **code.py** tiedostoon:

![](./img/vs_code_syntax_highlight.png "Visual Studio Code värit tekstieditorissa")

Huomaa miten VS Code automaattisesti värittää varatut sanat, funktioiden kutsut ja kommentit.

Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Koodin kirjoittaminen, yhteenveto**
Syy miksi ohjelmointikieliä on ylipäätään olemassa on se että ihmiset voisivat helpommin lukea ja ymmärtää lähdekoodia. Ohjelmoijan tehtävä on tuottaa luettavaa koodia, annettujen nimeämiskäytäntöjen puitteissa.

Kommentoinnissa ei tarvitse mennä liiallisuuksiin, mutta hyvin kommentoitu koodi on mukavaa luettavaa itselle ja muille.

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

