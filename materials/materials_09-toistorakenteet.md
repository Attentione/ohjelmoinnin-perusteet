# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Toistorakenteet**

[Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md): **/examples/09-loops/loops.py**

***while*** silmukka on yksinkertainen toistorakenne jota suoritetaan niin kauan kun sen ehtolause on tosi.

    number = 10
    while number >= 0:
        print(number)
        number -= 1

Huomaa että toistorakenteiden ehtolauseena voi käyttää mitä tahansa ehtolausetta, mukaanlukien yhdistämisiä ***and***- ja ***or*** operaattoreilla.

    number = 10
    while number >= 0 and number < 100:
        print(number)
        number -= 1

***for***-silmukkaa käytetään käymään läpi haluttu sekvenssi joka voi olla lista, tuple, sanakirja, joukko tai merkkijono.

Python kielen for silmukka on yksinkertaisempi kuin muiden ohjelmointikielten ja toimii kuin iteraattorimenetelmä muissa olio-ohjelmointikielissä.

***for***-silmukan avulla voimme suorittaa joukon lauseita, kerran jokaiselle listan, tuplen jne. kohteelle.

    for i in range(10):
        print("Looping range(10):", i)

Yllä oleva ***for*** silmukka käyttää varattua sanaa ***range*** näärittelemään kuinka monta kertaa silmukan koodi suoritetaan. ***range*** alkaa oletuksena nollasta, ja tässä sinne annettu parametri kertoo kuinka monta kertaa silmukka suoritetaan (10). Huomaa että ***range*** alkaa nollasta ja koodi suoritetaan 10 kertaa, niin tulostukseen tulee numerot 0-9.

***range***lle voi myös määrittää erikseen lähtö- ja loppuarvo:

    for i in range(5, 10):
        print("Looping range(5,10):", i)

Yllä oleva esimerkki toistaa i:n arvot 5-9. Oletuksena ***range*** kasvattaa arvoa yhdellä per silmukka. ***range***lle voi antaa myls kolmannen parametrin joka kertoo kuinka paljon arvoa kasvatetaan jokaisella silmukan kierroksella:

    for i in range(0, 10, 2):
        print("Looping range(0,10,2):", i)

***for*** silmukalla voi käydä läpi merkkijonon merkit:

    for c in "Basics of programming with Python":
        print(c)

Ja ***for*** silmukalla voi käydä läpi listamuuttujan elementtejä. Tuple- muuttujien läpikäynti toimii samalla tavalla:

    names = [ "John", "Cherry", "Jack" ]
    for name in names:
        print(name)


## **Break ja continue**

***while*** ja ***for*** toistolauseiden sisällä on mahdollista käyttää ***break*** ja ***continue*** kontrollilauseita.
- break
    - katkaisee silmukan suorituksen ja poistuu silmukkakoodista suorittamatta silmukan loppua.
- continue
    - aloittaa toistorakenteen seuraavan kierroksen.
    - ohittaa loput silmukkakoodista suorittamatta sitä.

Esimerkki ***break*** ja ***continue*** käytöstä toistorakenteessa:

    # use of break and continue
    print("Running a while loop with break and continue")
    number = 0
    while True:
        number = int(input("Enter a number (0 to exit, 100 ignored) "))
        if number == 0:
            # use break to jump out of the loop without checking its condition
            # the rest of the code after 'break' is not executed
            break
        elif number == 100:
            print("Ignored")
            # use continue to jump back to to beginning of the loop without
            # executing the rest of the code in loop
            continue

        print("You entered: ", number)



Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Toistorakenteet, yhteenveto**
Toistorakenteet on tehokas tapa suorittaa koodia useita kertoja. Kaikissa ohjelmissa tarvitaan toistorakenteita tarkastamaan taulukoiden sisältöä, hallitsemaan ohjelman valintoja ja ***pääsilmukkaa***. 

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

