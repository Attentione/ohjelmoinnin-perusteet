# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Versionhallinta**
Jokainen ohjelmistoprojekti tarvitsee versionhallinnan koska:
- Pystytään palaamaan takaisin aikaisempiin versioihin
- Voidaan verrata tehtyjä muutoksia ja nähdään muutosten tekijät
- Useat henkilöt voivat työskennellä saman projektin ja jopa samojen tiedostojen parissa samanaikaisesti
- Projektin tiedostot voidaan tallentaa hajautetusti ja tietojen katoaminen on erittäin epätodennäköistä


## **Git**
[Git](https://git-scm.com/) versionhallintaohjelmisto julkaistiin ensimmäistä kertaa vuonna 2005. Sen on kehittänyt Linus Torvalds Linux ytimen kehitystyötä varten.
Git on hajautettu ja tehokas versionhallintaohjelmisto joka mahdollistaa suurien kehitystiimien toimimisen saman projektin parissa.


## **Muita versionhallintaohjelmistoja**
Keskustelua/tutkimustyötä:
- Mitä versionhallintatyökaluja tunnet tai olet käyttänyt?
- [Versionhallinta Wikipediassa](https://fi.wikipedia.org/wiki/Ohjelmiston_versiohallinta)


## **Git Visual Studio Codessa**
Git integroituu VS Codeen kattavasti ja on helppo käyttää. VS Codeen on myös olemassa lisä-osia jotka helpottavat esim. GitHub- tai GitLab palveluihin kytkeytymistä. Tiedostojen versioiden vertailu on myös toteutettu VS Codessa erinomaisesti, ja muutosten hallinta on helppoa.

Tällä kurssilla opettelemme kuitenkin Git työkalujen peruskäytön komentoriviltä ja käytämme VS Codea versionhallintaan siinä määrin kun se on kätevää lähdekoodin muokkauksen kannalta. Kun hallitset Gitin peruskäytön komentoriviltä, voit käyttää sitä missä tahansa kehitysympäristössä, projektissa ja käyttöjärjestelmässä täysin samalla tavalla.


## **Gitin asennus ja käyttöönotto**
Git- ohjelmiston asennus tehtiin jo aikaisemmin. Mikäli et ole vielä asentanut tarvittavia ohjelmia kts. [Kehitysympäristö](./03-kehitysympäristö.md)

Käynnistetään Git Bash komentorivi:
- Windowsin tiedostonhallinnassa klikkaa oikealla napilla haluttua kansiota ja valitse **Git Bash Here**
    - Git Bash komentorivi aukeaa niin että polku on automaattisesti halutussa kansiossa
    - ![](./img/git-bash-here.png "Git Bash Here toiminnon käyttö Windows resurssienhallinnasta")

Oletusasetuksilla asennettu Git toimii myös Windowsin normaalissa komentorivitulkissa. Tällä kurssilla käytämme sitä kuitenkin aina Git Bash komentoriviltä koska silloin annetut komennot ja polut toimivat samalla tavalla kuin muissa käyttöjärjestelmissä.

**Huom**
Kun käytät ihan ensimmäisen kerran gitia, täytyy gitille kertoa kuka kommitteja tekee. Tai jos saat tällaisen ilmoituksen gitiltä: *Please tell me who you are." 
Joten ennen kuin teet ensimmäisen kommitin, anna seuraavat komennot:

```
git config --global user.email "sinunopiskelijanumerosi@student.jamk.fi"

git config --global user.name "Sinun Nimesi"
```

Tiedot tallentuu kyseisen tietokoneen git-asetuksiin, ja on siis konekohtainen. 


## **Git, perustyöskentely**
Git versionhallinnassa on käsitteet **remote repository** ja **local repository**. Repositoryllä tarkoitetaan yleisesti *tietovarastoa* ja versionhallinnassa tietovarasto voi olla esim pilvipalvelussa tai muulla palvelimella jolloin se on **remote**. Jokaisella versionhallinan käyttäjällä on tietovarastosta oma kopio joka sijaitsee käyttäjän omalla koneella, jolloin se on **local**. Muutokset näiden tietovarastojen välillä tehdään ***push*** ja ***pull*** toimintojen avulla:

![](./img/git-remote.png "Git versionhallinta paikallinen ja etä-repository")


Suuremmissa 'tosielämän' projekteissa Git versionhallinnan kanssa työskennellään pääpiirteissään seuraavalla tavalla:
- Kloonataan (clone) tai haarukoidaan (fork) projekti.
1. Päivitetään projekti ajan tasalle (pull).
2. Aloitetaan uuden ominaisuuden kehittäminen tekemällä sille oma haara versionhallintaan (branch).
3. Tarvittaessa tiedostoja lisätään (git add) tai poistetaan. Tehdään muutoksia tiedostoihin.
4. Muutokset varmistetaan versionhallintaan (git commit).
5. Iteroidaan kohtaan 3 niin kauan kunnes muutos on valmis.
6. Tehdään yhdistämispyyntö (Merge Request). Merge Requestit käsittelee yleensä joku muu kehitystiimin jäsen.
7. Merge Request käsitellään ja uuden ominaisuuden muutokset sulautetaan projektin pää-haaraan (master).
8. Aloitetaan uuden ominaisuuden tekeminen (kohta 1.).

Tällä kurssilla käytämme hieman yksinkertaistettua mallia Git-työskentelystä jossa jätämme *branchäyksen* pois. Tämä malli soveltuu hyvin silloin kun projektin parissa ei työskentele useampi henkilö.
- Kloonataan (clone) tai haarukoidaan (fork) projekti.
1. Päivitetään projekti ajan tasalle (git pull).
2. Aloitetaan uuden ominaisuuden kehittäminen.
3. Tarvittaessa tiedostoja lisätään (git add) tai poistetaan. Tehdään muutoksia tiedostoihin.
4. Muutokset varmistetaan versionhallintaan (git commit).
5. Iteroidaan kohtaan 3 niin kauan kunnes muutos on valmis.
6. Kun uusi ominaisuus on valmis, varmistetaan että commit on tehty ja päivitetään tiedostot GitLab palveluun (git push).
7. Aloitetaan uuden ominaisuuden tekeminen (kohta 1.).


## **JAMK GitLab palvelun käyttö**
Aloitetaan tekemällä uusi projekti GitLab palveluun.
1. Avaa [JAMK GitLab](https://gitlab.labranet.jamk.fi) ja kirjaudu sisään.
2. Valitse ylävalikosta ***Projects->Your projects***
3. Paina vihreää ***New Project*** nappia.
4. Anna projektille nimi **TTC2030**.
    - Koska käytämme tätä projektia kurssin materiaalien tallettamiseen, nimetään se kurssitunnuksen mukaan TTC2030
5. Merkkaa ***Initialize repository with a README***
    - Näin projektista ei tule tyhjä, vaan siellä on jotakin valmiina kun *kloonaamme* projektin komentoriviltä.
6. Paina vihreää Create project nappia.
7. Lisää opettajallesi (joko Jani, Hannu tai Esa) Developer-oikeudet projektiisi (katso alla oleva ohje)

![](./img/newproject1.png "Uuden projektin luonti GitLabiin")

## **JAMKin GitLabin projektin oikeudet opettajalle**
Lisäät opettajalle oikeudet projektiisi/repoosi seuraavasti
1. Oikean laidan valikosta valitse 'Members'
2. Kirjoita sinun opettajasi nimi kohtaan 'GitLab member or Email address' 
Huom: GitLabin pitäisi tuntea opettajat, joten valitse oikea nimi listasta jota GitLab-tarjoaa!
3. Kohtaan 'Choose a role permission' valitse 'Developer'
4. Paina 'Invite'

![](./img/newproject2.png "Oikeudet opettajalle projektiin")

## **Git Bash käyttö komentoriviltä**
1. Mene GitLab palveluun, avaa äskettäin luotu uusi projekti, ja paina ***Clone*** nappia
2. Alaikkuna aukeaa jossa on kaksi linkkiä, ***Clone with SSH*** ja ***Clone with HTTPS***
    - Voit halutessasi luoda SSH avaimet jolloin voit käyttää ***Clone with SSH*** toimintoa. Silloin sinun ei tarvitse käyttää käyttäjätunnusta ja salasanaa kun käytät Gittiä komentoriviltä. Katso tämän sivun lopusta ohje miten SSH avaimet voi luoda.
    - Näissä esimerkeissä me käytämme ***Clone with HTTPS*** jolloin meidän ei tarvitse luoda SSH avaimia.
3. Kopioi ***Clone with HTTPS*** laatikon teksti leikepöydälle.
4. Luo uusi kansio jonka sisään git projektisi kansio kloonataan. Tässä esimerkissä Z:\projects\
5. Valitse uusi kansiosi hiiren oikealla napilla ja valitse valikosta ***Git Bash Here***

Git Bash komentorivi-ikkuna aukeaa suoraan valittuun kansioon.

Nyt otetaan kopio GitLab projektista ***git clone*** komennolla. Kirjoita ***git clone*** ja sitten liitä (oikea hiirennappi + paste tai Shift+Insert) aikaisemmin GitLab palvelusta kopioimasi HTTPS linkki.

**Huom! Älä käytä Ctrl+V näppäinyhdistelmää koska bashissa Ctrl on erikoismerkki joka tulee näkymättömänä mukaan liitettyyn tekstiin.**

Huomaa alla olevassa esimerkissä *käyttäjänimi* jonka täytyy olla oma GitLab käyttäjänimesi.

HTTPS linkkiä käytettäessä git kysyy salasanaasi. SSH avaimia käyttämällä sinun ei tarvitse syöttää salasanaasi kloonaamisen yhteydessä.

    git clone https://gitlab.labranet.jamk.fi/käyttäjänimi/ttc2030.git

Mikäli käytät Gittiä ensimmäistä kertaa omalta koneeltasi, git saattaa kieltäytyä clone komennon suorittamisesta:

    *** Please tell me who you are.

Mikäli näin käy, suorita Gitin ehdottamat kaksi komentoa omilla tiedoillasi:

    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"


### **HTTPS salasana meni väärin**
Jos HTTPS salasana tuli kirjoitettua väärin, Windows ei anna uutta mahdollisuutta kirjoittaa käyttäjätunnusta ja salasanaa vaan tallettaa kirjautumistiedot riippumatta niiden oikeellisuudesta.

Että saat uuden mahdollisuuden kirjoittaa käyttäjätunnus tai salasana oikein, sinun on poistettava kirjautumistiedot Windowsin credentional manager toiminnolla.

Paina Windows näppäintä ja kirjoita ***credential***. **Best Match** osuudessa pitäisi näkyä ***Credential Manager***. Valitse se.

Valitse ***Windows Credentials*** ja alas listalle ilmestyy gitlab palveluun liittyvät tallennetut tunnukset. Avaa tunnus sen oikeassa reunassa olevall nuolella ja klikkaa tietojen alareunaan ilmestyvää Remove linkkiä.

![](./img/https_pwd_wrong_reset.png "HTTPS sanasanan tallennuksen resetointi")

Nyt voit kokeilla tehdä projektin kloonaamisen uudestaan.


## **Projektin tilan tarkastaminen git status komennolla**
git clone komento kopioi projektin tietokoneelle. Huomaa että koneelle luodaan git-projektin niminen kansio jossa projektin tiedostot sijaitsevat. Käytä cd komentoa siirtyäksesi projektin kansioon. ls komennolla voit listata kansion tiedostot.

Kun olet siirtynyt projektikansioon, voit tarkastaa projektin tilan git status komennolla.

    cd ttzc0200
    ls –la
    git status


## **Muokataan README.md tiedostoa**
Aluksi on hyvä laittaa Windowsin asetukset niin että näemme kaikki tiedostot oikein tiedostonhallinnassa.
- Avaa tiedostonhallinta (File Explorer)
- Valitse View valikko, ja oikeasta reunasta Options
- Folder Options ruudulla, valitse View välilehti
- Advanced Settings asetuksissa valitse
    - Show hidden files, folders, and drives
    - Ota ruksi pois asetuksesta Hide extensions for known file types
- Paina ylhäällä olevaa Apply to Folders nappia, niin Windows laittaa nämä asetukset kaikille kansioille.
- Vahvista muutokset (Yes) ja paina lopuksi OK

![](./img/windows_folder_options.png "Windows kansion asetukset")

Kokeillaan ensin muokata projektin README.md tiedostoa. Näin saamme varmistettua että kaikki toimii tietokoneesi ja GitLab palvelun välillä oikein.
- Avaa README.md tiedosto johonkin tekstieditoriin. Notepad käy mainioisti. Voit myös muokata sitä VS Codessa.
- Lisää README.md tiedostoon uusi otsikko ja hieman tekstiä

README.md tiedostoon:

    # TTC2030
    
    ## Tervetuloa
    Tervetuloa ohjelmoinnin perusteet kurssin etusivulle.

Palaa git bash komentoriville ja kirjoita

    git status

Huomaa että git listaa ‘Modified’ osuudessa muuttuneet tiedostot. Seuraavaksi varmistamme (commit) että haluamme tallentaa muutokset versionhallintaan.

    git commit -a -m "Modified the README.md file"

- commit komennon parametri –a (all) tarkoittaa että varmistetaan kaikki muokatut tiedostot.
- Parametri –m (message) jälkeen lisätään viesti jolla tämän commitin voi tunnistaa myöhemmin.

Nyt muokatut tiedostot on varmistettu, mutta nämä muutokset ovat vieläkin ainoastaan paikallisella koneella. Voit puskea muutokset myös GitLab verkkopalveluun ***push*** komennolla.

    git push

Nyt kaikki muutokset on päivitetty myös GitLab palveluun. Voit käydä katsomassa GitLab verkkosivulla että projektisi README.md on päivittynyt.


## **gitignore tiedoston käyttö**
.gitignore tiedostoon voit määritellä tiedostojen ja kansioiden nimiä joita et halua mukaan versionhallintaan. Tällaisia ovat tyypillisesti väliaikaistiedostot tms. tiedostot joita ei ole järkevää säilyttää versionhallinnassa.

1. Luo uusi tekstitiedosto Git projektin juureen nimeltä ***.gitignore***
    - Huomaa tiedoston nimen edessä oleva piste
    - Huomaa myös että ***.gitignore*** tiedostonnimessä ei saa olla jälkitunnistetta (esim .txt)
        - Varmista että näät Windowsissa myös tiedostotunnisteet
2. Avaa ***.gitignore*** tiedosto tekstieditorissa ja lisää kansioiden ja tiedostojen nimet joita et halua mukaan versionhallintaan
    - Voit käyttää myös villikortteja *, esim *.obj
    - Python tulkki saattaa joissakin tapauksissa luoda kansion *\_\_pycache\_\_*. Kokeillaan lisätä se kansio gitignoreen:

.gitignore tiedoston sisältö:

    __pycache__

3. Tallenna ***.gitignore*** tiedosto. Tämän jälkeen ***.gitignore*** tiedostossa nimettyjä kansioita ei oletuksena lisätä versionhallintaan.


## **Lisätään Hello projekti Git- versionhallintaan**
Mene Windowsin resurssienhallinnalla Git projektin juurikansioon (tässä esimerkissä käytetty Z:\projects\TTC2030\)

Luo sinne uusi kansio ***examples*** (Z:\projects\TTC2030\examples\)

Mene ***examples*** kansion sisään ja luo sinne uusi kansio ***hellopython*** (Z:\projects\TTC2030\examples\hellopython\)

Etsi resurssienhallinnalla [aikaisemmin tekemäsi](./04-ensimmainen-python-ohjelma.md) ensimmäinen Python-ohjelma ja kopioi ***hellopython.py*** tiedosto äsken luomaasi /examples/hellopython/ kansioon.

Avaa Git Bash komentotulkki Git projektin juurikansioon
    - Etsi resurssienhallinnalla juurikansio (tässä esimerkissä käytetty Z:\projects\TTC2030\)
    - Klikkaa kansiota hiiren oikealla napilla ja valitse ***Git Bash Here***

Katso ***status*** komennolla mitä projektissasi on muuttunut:

    git status

Huomaa että Git raportoi ***untracked files*** osuudessa että kansio nimeltään ***examples*** ei ole mukana versionhallinnassa. Mikäli uusi kansiosi ei listauksessa näy, varmista että sinulla on vähintään yksi tiedosto kansioiden sisällä. Tyhjää kansiota ei voi lisätä *git add* komennolla.

Lisätään kansio ja kaikki sen sisältö versionhallintaan ja tarkastetaan tila:

    git add examples/*
    git status

Nyt Git raportoi että versionhallinnassa on uusi tiedosto (***new file: hellopython.py***)
Nämä muutokset täytyy myös varmistaa, tähän käytetään ***commit*** komentoa:

    git commit -a -m "Added hello python to project"

***commit*** komennon parametri –a (all) tarkoittaa että varmistetaan kaikki muokatut tiedostot. Parametri –m (message) jälkeen lisätään viesti jolla tämän commitin voi tunnistaa myöhemmin.

Nyt uudet tiedostot on lisätty ja varmistettu, mutta nämä muutokset ovat vieläkin ainoastaan paikallisella koneella. Voit puskea muutokset myös GitLab verkkopalveluun ***push*** komennolla:

    git push

Nyt kaikki muutokset on päivitetty myös GitLab palveluun. Voit käydä katsomassa GitLab verkkosivulla että uudet projektitiedostot näkyvät myös siellä.

Pidä Git Bash ikkuna auki koska seuraavaksi teemme muokkauksia projektiin.


## **'Hello' projektin muokkaaminen**
Käynnistä VS Code ja avaa ***Open Folder...*** toiminnolla examples/hellopython kansio.
Muokataan hellopython.py tiedostoa seuraavasti:
- Muuta **msg** muuttujan alustukseen jotain muuta tekstia.
- Lisää koodia joka lukee käyttäjän syöttämää tekstia ja kirjoittaa sen takaisin konsoliin:

Uusi koodi:

    # read text from console and echo back
    text = input('Kirjoita jotain tekstiä: ')
    print("Kirjoitit: ")
    print(text)

Suorita ohjelma (Ctrl+F5) ja samalla varmistat että muutokset toimivat.

Tarkasta ***status*** komennolla mikä on projektin versionhallinnan tila. Mene Git Bash ikkunaan ja kirjoita:

    git status

Huomaa että Git raportoi että hellopython.py tiedosto on muuttunut ***modified: hellopython.py***. Nyt muutokset täytyy vielä commitoida ja 'pushata' GitLab verkkopalveluun:

    git commit -a -m "Lisäsin hellopython.py tiedostoon tekstisyötteen lukemisen"
    git push


## **Muutosten tarkastelu VS Codessa**
Nyt meillä on versionhallinnassa kaksi eri versiota hellopython.py tiedostosta. Kokeillaan miten voidaan käyttää VS Code ympäristön versionhallintaominaisuuksia:
- Avaa VS Code ja hellopython kansio.
- Muokkaa ***msg*** muuttujaan laitettavaa tekstiä
- Tallenna muutokset  (Ctrl+S)
- Avaa VS Coden ***Explorer*** (Ctrl+E) ja valitse hellopython.py tiedosto
- Explorer näkymän alareunassa on valinta ***Timeline***, avaa se
    - ***Timeline*** näkymässä on listattu allekkain kaikki hellopython.py tiedoston eri versiot.
    - Klikkaamalla eri versioita näet mitä muutoksia tiedostoon on tehty.
    - ***Uncommitted Changes*** versio näyttää muutokset joita ei ole vielä viety versionhallintaan.

![](./img/vs_code_timeline.png "Visual Studio Code Timeline")


## **Git, perustyöskentely**
Kun Git versionhallintaprojekti on luotu, yksinkertaisimmillaan perustyöskentely on seuraavanlaista. Jos olet työskennellyt projektin parissa toisella tietokoneella, tai projektissa työskentelee myös muita henkilöitä, täytyy muistaa ’vetää’ (***pull***) uusimmat muutokset paikalliselle koneelle ennen kun aloitat muokkausten tekemisen

    git pull
    ... muokataan paikallisia tiedostoja ...
    git add ... [tarvittaessa lisätään uudet tiedostot tai kansiot]
    git commit –a –m "Very descriptive comment for this commit"
    git push


## **Git, vinkkejä**

### **GitLab, SSH avainten luonti ja asetus**
GitLab tunnistautumisen voi tehdä myös SSH avaimilla. SSH tunnistautumista varten täytyy luoda SSH avainpari.
1. Avaa git bash komentorivityökalu
    - valitse kansio oikealla napilla ja ***Git Bash Here***
2. Luo uusi SSH avainpari ja kopio julkinen avain leikepöydälle
    - ssh-keygen -t ed25519 -C email@example.com
    - cat ~/.ssh/id_ed25519.pub | clip
3. Avaa [JAMK GitLab](https://gitlab.labranet.jamk.fi) ja kirjaudu sisään.
4. Klikkaa oikean ylänurkan avatarkuvaasi ja valitse ***Settings***
5. Vasemmalla valitse ***SSH Keys***
6. Liitä avain leikepöydältä ***Key*** kenttään ja lisää tunnistettava nimi ***Title*** kenttään. Tämän jälkeen paina ***Add key*** nappia.
7. Nyt voit käyttää GitLabin ***Clone with SSH*** toimintoa.


Tässä muutamia vinkkejä git komentorivikäyttöön:

Peru tiedoston paikalliset muutokset (esim readme.md tiedosto)

    git checkout readme.md

Peru kaikki paikalliset muutokset ja palaa viimeisimpään committiin

    git checkout .
 
Katso paikalliset muutokset komentorivillä (esim readme.md tiedosto)

    git diff readme.md

Katso tiedostoon tehdyt commitit ja niiden tunnisteet (esim readme.md tiedosto)

    git log readme.md

Palaa johonkin aikaisempaan committiin

    git checkout <commit-tunniste>

Miten päästä ulos vim-editorista!!! ;)

    ESC
    :!qa
    ENTER


## **Versionhallinta, yhteenveto**
Versionhallintaan on olemassa useita työkaluja, mutta Git on viime vuosina noussut johtavaan asemaan. Sen peruskäyttö on taito jota jokaisen ohjelmoinnin parissa oletetaan osaavaan. Lisäksi Git:in osaaminen avaa mahdollisuuden käyttää tuhansia vapaan lähdekoodin projekteja GitHub ja GitLab tyylisissä palveluissa.

Katso myös Karo Saharisen GIT kurssi:

http://gitlab.labranet.jamk.fi/sahka/gitlab-opintojakso

https://www.youtube.com/watch?v=zwk0rLEQ6Jw&feature=youtu.be

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

