# **Ohjelmoinnin perusteet**
TTC2030 - 5 op


## **Ensimmäinen Python ohjelma**
Tehdään ensimmäinen Python ohjelma VS Coden kehitysympäristössä:
1. Tee uusi kansio projektiasi varten. Tässä esimerkissä käytetään kansiota **C:\python\examples\hellopython**
2. Käynnistä VS Code ja avaa kansio **'File->Open Folder...'** toiminnolla
3. Lisää uusi tiedosto VS Code projektiin
    - Käytä ***New File*** nappia:
        - ![](./img/vs_code_add_source_file.png "Lisää uusi tiedosto projektiin")
    - Anna tiedostolle nimeksi **hellopython.py** ja VS Code avaa tiedoston muokkaamista varten
    - Saatat saada viestin VS Codelta **'Linter pylint is not installed.'**. Voit asentaa sen tai valita **'Do not show again'**. Linterin avulla voidaan tarkastaa lähdekoodin selkeyttä automaattisesti. Tällä kurssilla emme käytä lintteriä.


## **Lähdekoodin kirjoittaminen**
Kirjoita **hellopython.py** tiedostoon lähdekoodi:

    # import a datetime libray to access current date and time
    import datetime
    
    # declare a variable named 'msg' and set some text into it
    msg = "Tämän ohjelman suoritti Jani Immonen"
    
    # print the message into console
    print(msg)
    
    # print the current date and time
    print(datetime.datetime.now())

- '***#***' merkki aloittaa python kielessä kommenttirivin
- ***import*** sanalla voidaan ottaa käyttöön jokin Pythonin kirjastoista
    - Python kielessä on useita sisäänrakennettuja kirjastoja jotka helpottavat monia ohjelmointitehtäviä. Tässä esimerkissä tulostetaa tämänhetkinen päiväys ja kellonaika
    - ***import datetime*** Ottaa käyttöön **datetime** kirjaston ***import*** komennon avulla
    - **datetime** kirjastosta löytyy **datetime** niminen luokka
    - **datetime** luokasta löytyy **now** niminen funktio (toiselta nimeltään ali-ohjelma)
- ***msg = ...*** esittelee muuttujan nimeltä ***msg*** ja asettaa muuttujan arvoksi tekstiä
- ***print(msg)*** tulostaa msg muuttujassa olevan tekstin
- ***print(datetime.datetime.now())*** tulostaa tämänhetkisen päiväyksen ja kellonajan


## **Ohjelman ajaminen**
Voit ajaa ohjelman VS Codessa painamalla oikean yläreunan 'nuoli' painiketta, tai vaihtoehtoisesti painamalla Ctrl+F5:

![](./img/vs_code_run_python.png "Aja Python ohjelma VS Codessa")

VS Code avaa lähdekoodi-ikkunan alapuolelle ***'Terminal'*** näkymän johon ohjelman ***print*** komennot tulostuvat

Voit ajaa Python lähdekoodin myös ilman VS Codea Windowsin komentotulkissa:
1. Avaa komentotulkki (Win+R - cmd) ja siirry ***cd*** komennoilla kansioon jossa projektisi on
2. Kirjoita komento ***py hellopython.py***
3. Ohjelma suoritetaan ja sen tulostus ilmestyy komentotulkkiin


## **Ensimmäinen Python ohjelma, yhteenveto**
Näin pienellä vaivalla saimme ensimmäisen Python ohjelman tehtyä. Seuraavaksi opiskelemme Git- versionhallinan käyttöä.


&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

