# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Funktiot**
Olet ehkä kuullut termit funktio, metodi ja proseduuri. Kaikki ovat periaatteessa sama asia; kutsuttava aliohjelma.
- funktio
    - pala ohjelmakoodia jota kutsutaan sen nimellä. Funktiolle voi viedä muuttujia parametreinä ja se voi palauttaa tietoa.
- metodi
    - pala ohjelmointikoodia joka on määritelty osaksi luokkaa. Metodi voi käsitellä luokan muuttujia, sille voi viedä muuttujia parametreinä ja se voi palauttaa tietoa.
- proseduuri
    - on periaatteessa funktio joka ei palauta arvoa.

Koska emme käsittele tällä kurssilla olio-ohjelmointia, kutsumme aliohjelmia yleisesti funktioiksi.

[Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md): **/examples/10-functions/functions.py**


## **Oman funktion esittely**
Funktio esitellään Python kielessä ***def*** varatulla sanalla. ***def*** sanan jälkeen tulee ohjelmoijan päättämä funktion nimi ja sen jälkeen suluissa funktion parametrit, tai vain tyhjät sulut mikäli funktiolle ei viedä parametrejä. Funktion esittely päättyy kaksoispisteeseen **:**. Esittelyä seuraa funktion toteutus joka täytyy sisentää eteenpäin neljän välilyönnin verran. VS Code osaa tehdä sisennyksen **TAB** näppäimen painalluksella.

    def print_info():
        print("Info")

Esiteltyä funktiota voi kutsua koodissa käyttämällä funktion nimeä ja sen jälkeen funktion parametrit suluissa. Mikäli funktiolle ei viedä parametreja, on kutsussa silti oltava tyhjät sulut.

    print_info()

Funktiot voivat myös palauttaa arvon joka voidaan sijoittaa muuttujaan. Funktio palauttaa arvon ***return*** varatun sanan avulla ja palautettu arvo sijoitetaan muuttujaan ***=*** operaattorilla funktiokutsun yhteydessä.

    def print_and_return_number():
        print("Info - returning 123")
        return 123

    number = print_and_return_number()
    print("print_and_return_number returned ", number)

Seuraavaksi esitellään funktio jolle viedään parametreja ja se palauttaa arvon. Parametrit esitellään funktion nimen jälkeen sulkujen sisällä ja erotellaan pilkuilla.

    def sum(number1, number2):
        return number1 + number2

    number = sum(5, 11)
    print("sum returned ", number)

Python kielessä perustietotyypit kuten numerot ja merkkijonot viedään funktioparamerinä kopioina alkuperäisestä. Tämä tarkoittaa sitä että funktio käsittelee omia kopioitaan eikä muokkaa argumenttina lähetetyn muuttujan sisältöä. Kokeillaan tätä, lisätään uusi funktio **modify_text**:

    # define a function that takes text as a parameter and modifies
    # it before printing
    def modify_text(text):
        text += ", this text is added by function"
        print(text)

Ja lisätään kutsu **modify_text** funktioon:

    text = "About to call modify_text"
    modify_text(text)
    print(text)

Huomaa että argumenttina lähetetty **text** muuttuja ei ole muuttunut **modify_text** funktiokutsun jälkeen, eli **modify_text** funktion **text** parametri on oma kopio. Vaikka tällä kurssilla emme käsittele olio-ohjelmointia, on hyvä tietää että poikkeuksena tästä on *olioparametrit* jotka lähetetään oletuksena *viittausparametrina* jolloin funktio muokatessaan olioparametria muokkaa myös funktioon argumenttina lähetettyä oliota.


## **Toisessa tiedostossa esitellyn funktion käyttäminen**
Usein on kätevää jos omat funktiot voi esitellä toisessa lähdekooditiedostossa ja kutsua niitä toisesta. Tämä onnistuu ***from*** ja ***import*** varattujen sanojen avulla.

1. Tee uusi lähdekooditiedosto nimeltä ***helper.py***
2. Varmista että uusi lähdekooditiedosto on samassa kansiossa ***functions.py*** tiedoston kanssa.
3. Esittele funktio ***helper.py*** tiedostossa:

helper.py tiedoston sisältö:

    def sub(number1, number2):
        return number1 - number2

4. Lisää seuraava rivi ***functions.py*** tiedoston alkuun. Tämä rivi kertoo että haluamme ottaa käyttää kaikkia (*) helper.py tiedostossa esiteltyjä tietoja.

Lisätään ***functions.py*** tiedoston alkuun:

    from helper import *

5. Vaihtoehtoisesti voit määritellä yksittäisen funktion joka otetaan mukaan:

***functions.py*** tiedoston alussa (vaihtoehto):

    from helper import sub

6. Nyt voit kutsua ***helper.py*** tiedostossa esiteltyä ***sub*** metodia ***functions.py*** tiedostosta:

Lisätään ***functions.py*** tiedostoon:

    number = sub(5, 11)
    print("sub returned ", number)


### **Tuntitehtävä 1**
- Lue kaksi numeroa käyttäjältä (***input*** funktio).
- Käytä aikasemmin esiteltyä ***sum*** funktiota annettujen numeroiden summan laskemiseen.
- Tulosta summa konsoliin


### **Tuntitehtävä 2**
- Tee uusi funktio joka ottaa kaksi parametriä, etunimi ja sukunimi (***firstname***, ***surname***).
- Funktion tehtävä on palauttaa merkkijono jossa on palautettavassa merkkijonossa etunimi ja sukunimi eroteltuna välilyönnillä.
- Kutsu uutta funktiotasi ja tulosta sen palauttama merkkijono konsoliin.


Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Funktiot, yhteenveto**
Funktiot ovat tietokoneohjelman lihaksia. Funktiot:
- käsittelevät sinne lähetettyjä parametreja
- käsittelevät näkyvyysalueella olevaa informaatiota
- palauttavat tuloksia
- niitä kutsutaan toistolauseissa
- niitä kutsutaan ehtolauseissa
- kutsuvat muita funktioita
- voivat kutsua myös itseään (rekursio -> stack overflow!)

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

