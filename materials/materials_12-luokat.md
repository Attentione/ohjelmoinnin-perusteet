# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

**Huom:** Tämä kurssi ei käsittele syvällisemmin olio-ohjelmointia, mutta käytämme tällä kurssilla luokkia omien tietorakenteiden esittelyyn.

## **Omat tietorakenteet, Luokat**
Python tukee olio-ohjelmointia ja siinä voi määritellä **luokkia** (engl *class*).  Luokka on ohjelmoijan määrittelemä tietotyyppi. Luokka kapseloi sarjan muuttujia ja funktioita uuteen- ohjelmoijan määrittelemään muuttujatyyppiin. Luokka kertoo mitä tietoja ja toimintoja luokka sisältää ja sen jälkeen voimme esitellä tämän tyyppisiä muuttujia joita kutsutaan **olioiksi** (engl *object*).Luokkien ominaisuuksista käytetään termiä **ominaisuus** (engl *property*) ja toimintoja kutsutaan termillä **metodi** (engl *method*). 

Esimerkiksi:
- **Person** (Henkilö) on luokka jolla on ominaisuutena tiedot **name** (nimi) ja **height** (henkilön pituus).
    - John ja Mary ovat henkilöitä joten heillä molemmalla on nimi ja pituus.
    - Johnilla ja Maryllä on kuitenkin toisistaan poikkeava nimi ja pituus, joten molemmalla tiedot ovat erikseen tietokoneen muistissa.
        - Joten John ja Mary ovat olioita ja niiden molempien luokka on **Person**.

[Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md): **/examples/12-classes/classes.py**

## **Luokan määrittely**
Tee **/examples/12-classes/** kansioon uusi tiedosto **vehicle.py**. Luokka esitellään varatulla sanalla ***class***. Esitellään luokka joka pitää sisällään ajoneuvojen tietoja:

    # declare a Vehicle class to hold information about vehicle data
    class Vehicle:
        make = ""
        model = ""
        engine_cc = 0
        power_kw = 0


## **Olion esittely**
Nyt meillä on käytössä **Vehicle** luokka johon voi tallentaa tietoina ajoneuvon merkin, mallin, moottorin koon ja tehon kilowatteina. Nyt voimme luoda luokasta *olion*. Avaa **classes.py** lähdekooditiedosto ja lisää siihen seuraavat koodirivit:

    # import the Vehicle class from another source file
    from vehicle import Vehicle

    # declare an object 'car' from 'Vehicle' class
    car = Vehicle()


## **Olion tietojen muokkaaminen**
Yllä loimme **car** nimisen olion joka on **Vehicle** tyyppia. Voimme muokata car olion tietoja viittaamalla niihin pisteoperaattorin avulla.

    # declare an object 'car' from 'Vehicle' class
    car = Vehicle()
    car.make = "Datsun"
    car.model = "100A"
    car.engine_cc = 998
    car.power_kw = 12

    # print car info
    print(car)


## **Luokkatyypin muunnos merkkijonoksi**
Huomaa että ***print*** ei osaa tulostaa **car** olion tietoja koska **Vehicle** tietotyyppi on meidän määrittelemä. Muokataan **Vehicle** luokkaa **vehicle.py** tiedostossa niin että lisätään sinne funktio joka osaa tulostaa auton tiedot:

    # declare a Vehicle class to hold information about vehicle data
    class Vehicle:
        # override conversion from Vehicle to string with __str__
        def __str__(self):
            return self.make + " " + self.model + ", " + str(self.engine_cc) + "cc, " + str(self.power_kw) + "kw"

        make = ""
        model = ""
        engine_cc = 0
        power_kw = 0

Kun esittelimme ***__str__*** nimisen funktion **Vehicle** luokkaan, määrittelemme miten luokkamme tiedot tulee muuntaa tekstiksi. Kun nyt ajat **classes.py** tiedoston, huomaat että **car** olion tiedot tulostuvat meidän määrittelemällä tavalla. Luokan sisällä olevat funktiot käyttävät ***self*** varattua sanaa parametrina viittaamaan ja muokkaamaan omia muuttujia.


## **Luokan muodostaja**
Jos haluamme esitellä toisen **Vehicle** olion, vaikkapa **car2**, ja täyttää sen tiedot meidän pitäisi kirjoittaa 5 uutta koodiriviä (Olion luonti, merkin, mallin, moottorin koon ja tehon asettaminen). Mutta tähän on parempikin tapa; voimme esitellä luokalle *muodostajan* (***init*** function). Avaa **vehicle.py** tiedosto ja lisää **Vehicle** luokalle muodostaja:

    # declare a Vehicle class to hold information about vehicle data
    class Vehicle:
        def __init__(self, make="", model="", engine_cc=0, power_kw=0):
            self.make = make
            self.model = model
            self.engine_cc = engine_cc
            self.power_kw = power_kw

        # override conversion from Vehicle to string with __str__
        def __str__(self):
            return self.make + " " + self.model + ", " + str(self.engine_cc) + "cc, " + str(self.power_kw) + "kw"

        make = ""
        model = ""
        engine_cc = 0
        power_kw = 0

Nyt **Vehicle** luokalla on muodostaja joten voimme muuttaa olion luomisen yksinkertaisempaan muotoon. Avaa **classes.py** tiedosto ja muuta *car* olion luominen:

    # declare an object 'car' from 'Vehicle' class
    car = Vehicle("Datsun", "100A", 998, 12)

Voimme viedä ajoneuvon tiedot suoraan muodostajafunktiolle joka alustaa olion annetuilla argumenteilla. Nyt toisenkin ajoneuvo-olion lisääminen on helppoa:

    # declare an object 'car' from 'Vehicle' class
    car = Vehicle("Datsun", "100A", 998, 12)

    # declare another vehicle
    car2 = Vehicle("Toyota", "Celica", 1588, 43)

    # print cars
    print(car)
    print(car2)


# **Luokan omat metodit**
Voimme tarpeen mukaan lisätä luokkaan omia funktioita. Luokkien ja olioiden funktioita kutsutaan **metodeiksi** (engl method). Kokeillaan tehdä **Vehicle** luokalle metodi, joka palauttaa kyseisen ajoneuvon tehon hevosvoimina. Avaa **vehicle.py** tiedosto ja lisää *horsepower* metodi:

    def horsepower(self):
        return self.power_kw * 1.341

Seuraavaksi avaa **classes.py** tiedosto ja kokeile kutsua *horsepower* metodia:

    # use Vehicle.horsepower function to get power as hp
    print("car power is: ", car.horsepower(), "hp")
    print("car2 power is: ", car2.horsepower(), "hp")

Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Omat tietorakenteet, luokat yhteenveto**
Luokan avulla voimme esitellä omia tietotyyppejä, jotka rakentuvat muista tunnetuista tietotyypeistä. Luokat voivat sisältää metodeja, jotka käsittelevät olion tietoja. Luokkien avulla voimme mallintaa tosielämän rakenteita ja kääriä useita parametreja yhden käsitteen alle. Näin saamme koodista helposti ymmärrettävää.

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

