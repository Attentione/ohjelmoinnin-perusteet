# **Ohjelmoinnin perusteet**
TTC2030 - 5 op


## **Mitä on ohjelmointi**
### Pohdintaa ###
- Mikä on tietokoneohjelma?
- Mitä on ohjelmointi?
- Mitä ohjelmointikieliä tiedät/olet käyttänyt?
- Mitä skriptauskieliä tiedät/olet käyttänyt?
- Python tukee olio-ohjelmointia, Mitä muita [ohjelmointiparadigmoja](https://fi.wikipedia.org/wiki/Ohjelmointiparadigma) tunnet?


## **Tietokoneohjelma**
Tietokoneohjelma on sarja askel-askeleelta suoritettavia komentoja jotka ohjaavat tietokonetta tekemään halutut tehtävät.

On monta hyvää syytä opetella ohjelmointia:
- Ohjelmointi auttaa ymmärtämään tietokoneita ja ohjelmistoja.
- Muutaman ohjelman kirjoittaminen kasvattaa itseluottamusta tietokoneen käytössä.
- Ohjelmoinnin kokeileminen antaa nopeasti vastauksen onko se sinua varten. Siitä on helppo innostua.
    - Ja vaikka ei innostuisikaan, perusasioiden ymmärtäminen kasvattaa arvostusta käytettyjä ohjelmistoja kohtaan.


## **Ohjelmointi**
Ohjelmointi on tekniikka jolla ohjelmoija kertoo tietokoneelle sarjan komentoja miten sen tulisi suorittaa ohjelmaa ja käsitellä tietoa. Kielestä ja ympäristöstä riippumatta, ohjelmointi on periaatteessa aina samanlaista:
1. Käytetään tietokoneen muistia tiedon tallentamiseen
    - Tietotyypit ja muuttujat
2. Kirjoitetaan algoritmeja jotka muokkaavat tietoa
    - Funktiot (eli aliohjelmat), tietoluokat
3. Kirjoitetaan logiikka jonka perusteella algoritmeja käytetään tiedon muokkaamiseen
    - Ehto- ja toistorakenteet

Suomen kielen sana ’Tietokone’ on kehno käännös. Englannin kielen sana ’Computer’ on lähempäna totuutta. Tietokone on suorittaja, pohjimmiltaan vain glorifioitu laskin. Kaikki tieto mitä tietokone sisäisesti käsittelee ovat kokonais- tai liukulukuja, eli numeroita.

Miten tietokoneen käsittelemät numerot sitten näyttäytyvät ihmisille tekstinä, kuvina, videoina? Esimerkiksi tekstin näyttämiseen on sovittu yhteisesti [ASCII](https://www.garykessler.net/library/ascii.html) taulukko. ASCII taulukko määrittelee mikä numero vastaa mitä kirjainta. Esimerkiksi: ASCII taulukossa on sovittu että iso A kirjain on tietokoneen muistissa numero 65.

Sama sääntö pätee myös kaikkeen tietokoneen ruudulla näkyvään grafiikkaan. Näkemämme kuva koostuu pikseleista jotka ovat sovitulla tavalla määriteltyjä numeroita. Numerot muodostavat punaisen, vihreän, ja sinisen värin määrän yhdessä kuvapisteessä. Tietokoneen näyttö on laite joka muuttaa numeroita valoksi niin että lopulta siitä muodostuu kuva.

Tämä on erinomainen esimerkki siitä miten hyvin yksinkertaisesta asiasta (numero) voi juontua hyvinkin monimutkaisia tietorakenteita ja järjestelmiä. Mutta on myös tavallaan helpottavaa tietää että loppujen lopuksi kaikki tieto tietokoneen muistissa on vain numeroita joita tulkitaan erilaisilla ihmisen kehittämillä säännöillä.


## **Ohjelmointikielien historiaa (lyhyesti)**
- 1950 luku
    - Fortran (1957)
    - Algol (1958)
    - Lisp (1958)
    - Cobol (1959)
- 1960 – 1980 luvut
    - Basic (1964)
    - Simula (1965)
    - Pascal (1969)
    - C (1972)
    - Ada (1980)
    - Smalltalk (1980)
    - C++ (1983)
    - Perl (1987)
- 1990 -
    - Python (1990)
    - Java (1995)
    - Ruby (1995)
    - Gambas (1999)
    - C# (2000)
    - Scala (2003)
    - Clojure (2007)
    - Elixir (2011)


## **Mitä on ohjelmointi, yhteenveto**
Suosituimmat ohjelmointikielet: [Tiobe Index](https://www.tiobe.com/tiobe-index/)

Ohjelmointi on komentojen kirjoittamista tietokoneelle. Vaikka ohjelmointikieliä on paljon, tavoite siitä että tietokone suorittaa tehtäviä halutulla tavalla on aina sama.

Ohjelmointi voi olla erittäin mielenkiintoinen harrastus, haastava ura, tai pikainen tuttavuus matkalla jonnekin muualle. Harvoin se kuitenkaan on kenellekään sitä kokeilleelle merkityksetöntä.

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

