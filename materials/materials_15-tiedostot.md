# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Tiedostojen käsittely**
Lähes poikkeuksetta jokaisessa ohjelmassa tulee tarve tallettaa tietoja niin että ne ovat saatavilla ohjelman tai tietokoneen sammutuksen jälkeen. Ohjelmasi saattaa myös lukea toisten ohjelmien tuottamia tiedostoja kuten tekstiä ja kuvia.

[Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md): **/examples/15-files/files.py**

## **Tiedostoon kirjoittaminen**
Kun tiedosto luodaan ilman polkua, tiedosto luodaan oletuksena samaan kansioon ajettavan py tiedoston kanssa.

    filename = "files-example.txt"
    file = open(filename, "w")
    file.write("Creating a file with Python!")
    file.close()

Kun olet suorittanut yllä olevan koodin, **files-example.txt** niminen tiedosto on ilmestynyt /examples/14-files/ kansioon. Voit avata sen vaikka Notepadillä.

Kokeillaan kirjoittaa tiedosto suoraan C aseman juureen. Windows 10 ei pidä tästä ja ohjelma aiheuttaa PermissionError virheen.

    # trying to write a file directly to C drive will cause a PermissionError
    filename = "files-example.txt"
    try:
        file = open("C:\\" + filename, "w")
        file.write("trying to write to C root, not going to happen :(")
        file.close()
    except:
        print("Failed to create file to C-Root")


## **Käyttäjän kotikansio**

Koska emme voi kirjoittaa tiedostoja minne tahansa, ja lähdekoodikansioon kirjoittaminen ei ole aina haluttu, kirjoitamme koodin joka tekee uuden kansion käyttäjän kotihakemistoon. Kysymme ***os.path*** luokalta polkua käyttäjän kotihakemistoon ja luomme sinne uuden kansion johon tiedosto kirjoitetaan.

    import os.path

    # get the user home folder, create a new folder and write the file there
    path = os.path.expanduser("~/files-example")
    if not os.path.exists(path): os.makedirs(path)
    path += "/" + filename

    file = open(path, "w")
    file.write("Files example, writing file to users home folder")
    file.close()

Yllä oleva esimerkki ***os.path.expanduser*** funktiota rakentamaan polun joka osoittaa käyttäjän kotihakemistoon ja sinne **files-example** kansioon. Tämän jälkeen käytetään ***os.path.exists*** funktiota tarkastamaan onko kansio olemassa. Mikäli se ei ole se luodaan ***os.makedirs*** funktiolla. Tämän jälkeen kansio on luotu (Windowsissa) **C:\Users\SinunKäyttäjänimi\files-example** kansio.

Seuraavaksi koodi lisää kansiopolkuun tiedostonnimen ja avaa tiedoston kirjoittamista varten. Voit käydä etsimässä tiedoston Windowsin resurssienhallinnalla.


## **Tekstirivien kirjoittaminen tiedostoon**
Tiedostoon kirjoittaessa on käsiteltävä tapahtuneet virheet. Alla oleva esimerkki kirjoittaa tiedostoon useita tekstirivejä ja tarkastaa mahdolliset virhetilanteet. Huomaa kuinka ***finally*** blokkia käytetään sulkemaan tiedosto riippumatta onko poikkeusta tapahtunut.

    filename = "multiline-text.txt"
    lines = [ "First line\n", "Second line\n", "Third line\n" ]
    try:
        file = open(path + filename, "w")
        file.writelines(lines)
    except Exception as e:
        print("Failed to write file: " + filename)
    finally:
        file.close()


## **Tiedoston lukeminen**
Tämä esimerkkikoodi lukee edellisen esimerkin kirjoittamat tekstirivit takaisin muistiin.

    lines = [""]
    try:
        file = open(path + filename, "r")
        lines = file.readlines()
    except:
        print("Failed to read file: " + filename)
    finally:
        file.close()
        print(lines)


## **Omien tietotyyppien tallettaminen tiedostoon**
Perustietotyyppien kuten numeroiden ja merkkijonojen tiedostokäsittely on helppoa, mutta jossakin vaiheessa on tarpeen tallettaa ja ladata omia tietotyyppejä. Kokeillaan tätä aikaisemmin tekemällämme **Vehicle** luokalla. Kopioi **Luokat** luennossa tekemämme **vehicle.py** tiedosto samaan kansioon tämän luennon **files.py** tiedoston kanssa. Tämän jälkeen lisää **files.py** tiedoston alkuun **import** rivit joilla saamme käyttöön *pickle* kirjaston sekä **Vehicle** luokan:

    # import the pickle library to save/load custom datatypes to file
    import pickle

    # import the Vehicle class from another source file
    from vehicle import Vehicle

*Pickle* kirjasto mahdollistaa omien tietotyyppien *serialisoinnin* tiedostoon tai muuhun tietovarastoon. Serialisoinnilla tarkoitetaan tiedon siirtämistä muualle ja sen palauttamista. Seuraavaksi luodaan lista **Vehicle** olioita ja tulostetaan listan sisältö:

    # declare a list of Vehicle objects
    vehicles = []
    vehicles.append(Vehicle("Datsun", "Sunny", 1197, 29))
    vehicles.append(Vehicle("BMW", "M5", 4899, 294))
    vehicles.append(Vehicle("Toyota", "Supra", 2999, 200))
    vehicles.append(Vehicle("Plymouth", "Cuda", 6845, 597))

    # print the contents of vehicle list
    for vehicle in vehicles:
        print(vehicle)

Tämän jälkeen kirjoitetaan koodi joka tallettaa oliolistan binääritiedostoon:

    # write a list of Vehicles into the vehicles.dat
    # the pickle library will save the list objects in binary format
    # hence we use the 'dat' filename extension instead of txt
    # also note that output file is opened with "wb" (write binary) mode
    filename = "vehicles.dat"
    try:
        file = open(path + filename, "wb")
        pickle.dump(vehicles, file, pickle.HIGHEST_PROTOCOL)
    except:
        print("Failed to write file: " + filename)
    finally:
        file.close()

    # objects are now saved into the file, clear the list
    vehicles.clear()

Huomaa että avaamme tiedoston *wb* (write binary) tilaan joka tarkoittaa että tiedostoon kirjoitetaan binääridataa. Binääridatalla tarkoitetaan tietoa joka on suoraan numeraalista eikä sitä tyypillisesti voi lukea tekstinä. Binäärimuodossa tiedon voi tallettaa käyttämällä vähemmän tilaa ja tiedon lukeminen tiedostosta on myös nopeampaa.

Oliolistan kirjoitus tiedostoon tehdään **pickle.dump** funktiolla. Parametreina sinne viedään oliolista, kohdetiedosto, sekä tieto missä muodossa tiedot talletetaan. **pickle.HIGHEST_PROTOCOL** muoto on binäärimuoto joka on olioiden tallennukseen tehokkain optio.

Lopuksi lista tyhjennetään **clear** funktiolla. Nyt voimme kokeilla lukea tiedot takaisin listaan:

    # try to load the objects back from the file
    try:
        file = open(path + filename, "rb")
        vehicles = pickle.load(file)
    except:
        print("Failed to read file: " + filename)
    finally:
        file.close()

    # print list contents to confirm that objects were loaded into the list
    for vehicle in vehicles:
        print(vehicle)

Tiedosto avataan uudelleen, tällä kertaa *rb* (read binary) tilassa. Tietojen lataaminen tehdään **pickle.load** funktiokutsulla jonne viedään parametrina avattu tiedosto ja se palauttaa oliolistan. Lopuksi varmistetaan että tiedot ovat todellakin latautuneet tulostamalla oliolista.

Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Tiedostojen käsittely, yhteenveto**
Tiedostojen käsittely Python kielessä on varsin helppoa ja tehokasta. Voit helposti kirjoittaa ja lukea merkkijonoja tai itse määrittelemiäsi rakenteita.

On tärkeää muistaa käsitellä poikkeukset kun tiedostoja luetaan tai kirjoitetaan. Nykyaikaisissa käyttöjärjestelmissä tiedosto voi olla väliaikaisesti saavuttamattomissa.

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

