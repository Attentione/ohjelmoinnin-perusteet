# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Poikkeusten käsittely**

Ohjelmointivirheet voidaan jakaa karkeasti kolmeen eri tyyppiin.
1. Käännösvirheet
    - Lähdekoodissa on virhe joka estää ohjelmaa käynnistymästä
2. Ajonaikaiset virheet
    - Ohjelmassa tapahtuu ajon aikana jotain odottamatonta
3. Loogiset virheet
    - Ohjelma ei kaadu, mutta ei toimi niinkuin sen pitäisi toimia

Poikkeusten käsittelyn (Exception Handling) avulla ohjelmoija voi varautua ajonaikaisiin virheisiin ja halutessään käsitellä ne niin että ohjelman suoritus voi jatkua.

[Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md): **/examples/14-exceptions/exceptions.py**

Ajonaikaiset virheet tulee käsitellä niin että ohjelma ei kaadu vaan sen suoritus voi jatkua.

Tässä esimerkkinä perinteinen nollalla jako. Ohjelma pysähtyy ZeroDivisionError virheeseen eikä pysty enää jatkamaan suoritusta eteenpäin.

    number1 = 100
    number2 = 0
    result = number1 / number2
    print("Result is ", result)

![](./img/vs-code-zerodivisionerror.png "Visual Studio Code ilmoittaa ZeroDivisionError ajonaikaisesta virheestä")

Toinen esimerkki. Kysytään käyttäjältä lukua ja hän syöttääkin merkkejä. Ohjelma pysähtyy ValueError virheeseen eikä pysty enää jatkamaan suoritusta eteenpäin.

    number = int(input("Give a number: "))
    print("you entered: ", number)

Viimeisenä melko tyypillinen ohjelmointivirhe. Taulukosta yritetään lukea tai kirjoittaa alkiota jota ei ole olemassa. Ohjelma pysähtyy IndexError virheeseen.

    numbers = [ 1, 2, 3, 4, 5 ]
    print("Last number is ", numbers[5])

Edellä esiteltiin vain muutamia eri poikkeustyyppejä. Erilaisia poikkeustyyppejä on paljon ja ohjelmoija voi myös tehdä niitä itse lisää. Kaikkien poikkeuksien tarkoitus on antaa ohjelmoijalle mahdollisimman paljon informaatiota siitä mitä ohjelman suorituksessa on mennyt pieleen.

Käsittelemätön ja tapahtunut poikkeus aiheuttaa ohjelman suorituksen loppumisen (kaatumisen) ja virhe-ilmoituksen näyttämiseen.

Poikkeukset käsitellään seuraavilla avainsanoilla ja niitä seuraavilla koodilohkoilla.
- ***try***
    - lähdekoodi joka voi aiheuttaa poikkeuksen sijoitetaan ***try*** lohkoon.
- ***except***
    - mikäli poikkeus tapahtuu ***try*** lohkossa, ohjelman suoritus hyppää ***except*** lohkoon. Loppuosa ***try*** lohkossa olevasta koodista jää suorittamatta
- ***finally***
    - ***except*** lohkon jälkeen voidaan esitellä ***finally***-lohko joka suoritetaan tapahtui ***try*** lohkossa poikkeusta tai ei.
    - ***finally*** lohkon käyttäminen ei ole pakollista.


### **Nollalla jako, käsitellään poikkeus**
Korjataan esimerkki jossa nollalla jako aiheutti virheen ohjelman toiminnassa:

    try:
        number1 = 100
        number2 = 0
        result = number1 / number2
        print("Result is ", result)
    except ZeroDivisionError:
        print("Can't divide by zero!")


### **Vääräntyyppinen syöte, käsitellään poikkeus**
Korjataan esimerkki jossa tekstin asettaminen numeromuuttujaan aiheutti virheen ohjelman toiminnassa. Huomaa miten eri tyyppisiä poikkeuksia voidaan käsitellä peräkkäisissä ***except*** osioissa:

    try:
        number = int(input("Give a number: "))
        print("you entered: ", number)
    except ValueError:
        print("Unable to convert to number")
    except:
        print("Something else went wrong :(")


### **Toulukon ylivuoto, käsitellään poikkeus**
Korjataan esimerkki jossa taulukkoa yritettiin lukea alkiosta jota ei ollut olemassa.

    try:
        numbers = [ 1, 2, 3, 4, 5 ]
        print("Last number is ", numbers[5])
    except IndexError:
        print("Wrong index used in accessing list")


## **Poikkeusten heittäminen (raise)**
Poikkeuksia voi myös heittää itse. Tässä esimerkissä SafeDivision funktio tarkastaa jos jakaja on 0 ja aiheuttaa tarvittaessa virhetilanteen.

    def safe_division(x, y):
        if y == 0:
            raise Exception("Exception from safe_division")
        return x / y

    try:
        result = safe_division(100, 0)
    except:
        print("safe_division raised an exception")

Jos esittelet uuden funktion mutta jätät sen toistaiseksi toteuttamatta on hyvä merkitä se NotImplementedError poikkeuksella. Jos funktio unohtuukin toteuttamatta, huomaat sen helposti koska saat kehitysvaiheessa poikkeuksen kun yrität kutsua metodia.

    def someday_something_here():
        raise NotImplementedError("This function is not implemented currently")

    someday_something_here()


Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Poikkeusten käsittely, yhteenveto**
Poikkeusten oikea käsittely on tärkeää ettei ohjelmasi heitä hanskoja tiskiin liian kevyin perustein. Nykyaikaisissa käyttöjärjestelmissä monia asioita voi mennä pieleen ja ohjelmasi on pystyttävä selviämään niistä. Joitakin tosielämän esimerkkejä ovat:
- Käyttäjä syöttää vääräntyyppistä tietoa
- Tiedostoa ei voida avata koska joku muu ohjelma käyttää sitä
- Tiedostoa ei voi kirjoittaa koska levytila on vähissä
- Yhteys Internettiin voi olla huono tai katketa kokonaan

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

