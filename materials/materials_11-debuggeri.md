# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Debuggeri**

Debuggerin avulla voi pysäyttää ohjelman suorituksen ja tarkastella muuttujien arvoja, *kutsupinoa (call stack)* ja myös paljon muuta ohjelman suoritukseen liittyvää informaatiota. Visual Studio Code tukee Python ohjelmien debuggausta Python lisäosan avulla. Debuggerin käyttö auttaa ymmärtämään ohjelmasi toimintaa ja sen käytön opetteleminen maksaa käytetyn ajan taatusti takaisin.

[Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md): **/examples/11-debugger/debugger.py**


## **Pysäytyskohta**
Tehdään muutamia muuttujia ja funktioita joiden toimintaa voimme tarkkailla debuggerin avulla. Koodiesimerkissä otamme **import** varatulla sanalla käyttöön *random* kirjaston jonka avulla saamme satunnaislukuja. Sen jälkeen esittelemme ja alustamme kaksi muuttujaa sekä tulostamme niiden arvot.

    # use some random numbers to observe in debugger
    from random import *

    # declare couple of int variables
    # observe the number2 in debugger to see what number is assigned to it
    number1 = 10
    number2 = randint(0, 100)

    # print the values of our variables
    print("value of number1 is ", number1)
    print("value of number2 is ", number2)

Lisätään number1 muuttujan esittelyn riville pysäytyskohta. Pysäytyskohdan voi lisätä
* painamalla F9 kun kursori on halutulla rivillä
* klikkaamalla hiiren vasemmalla napilla tekstieditorin vasemmassa marginaalissa (rivinumeron vasemmalla puolella)


## **Debuggerin näkymät**
Käynnistä debuggaus F5 napilla (tai ylavalikko *Run->Start debugging*). VS Code pyytää valitsemaan kofiguraation:

![](./img/vs-code-debug-configuration.png "Visual Studio Code debuggering konfiguraation valinta")

Valitse *Python File*, jolla kerromme debuggerille että haluamme debugata tätä kyseistä Python tiedostoa.

Tämän jälkeen ohjelman suoritus alkaa ja se pysähtyy koodiriville jonne laitoimme pysäytyskohdan. Keltainen nuoli on ns. *instruction pointer* joka näyttää kohdan jossa ohjelman suoritus on sillä hetkellä.

![](./img/vs-code-debugger-breakpoint.png "Ohjelman suoritus on pysäytyskohdassa")

Huomaa että VS Code on automaattisesti vaihtanut debuggausnäkymään. Voit myös itse tarvittaessa vaihtaa debuggausnäkymään vasemman laidan napilla.

![](./img/vs-code-debugger-view.png "VS Code debuggausnäkymän nappi")

Debuggausnäkymässä on neljä osaa:
* *Variables*, eli muuttujat näyttää sillä hetkellä tunnetut muuttujat. Muuttujat on jaettu kahteen eri osaan:
    * *Locals* näyttää paikalliset, vain kyseisessä koodiblokissa käytössä olevat muuttujat.
    * *Globals* näyttää myös muissa tiedostoissa esitellyt muuttujat jotka ovat käytettävissä.
* *Watch* osaan voit halutessasi lisätä muuttujien nimiä joita haluat seurata debuggerissa.
* *Call stack* osassa on listaus kaikista funktioista joita on kutsuttu, mutta ne ei ole vielä palanneet.
* *Breakpoints* osa näyttää aktiiviset pysäytyskohdat sekä aktiiviset ja käsittelemättömät poikkeukset (*exception*). Käsittelemme poikkeuksia myöhemmin.


## **Askeltaminen ja muuttujien tarkastelu**
Voit kontrolloida debuggerin toimintaa ylös keskelle ilmestyvällä nappirivistöllä:

![](./img/vs-code-debugger-controls.png "VS Code debuggauskontrollit")

Nappien toiminnat ja pikanäppäimet ovat seuraavat:
* *Continue (F5)* jatkaa ohjelma suoritusta kun olemme pysäyhtyneet pysäytyskohtaan. Ohjelma pysähtyy seuraavaan pysäytyskohtaan tai sen suoritus tulee päätökseen.
* *Step Over (F10)* suorittaa koodirivin ja pysähtyy seuraavalle koodiriville.
* *Step Into (F11)* askeltaa funktion sisään jos pysäytyskohta on funktiokutsun kohdalla. Muuten *Step Into* toimii kuten *Step Over*
* *Step Out (Shift+F11)* suorittaa funktion loppuun ja pysähtyy funktiokutsua seuraavalle riville.
* *Restart (Ctrl+Shift+F11)* aloittaa debuggauksen uudelleen ohjelman alusta.
* *Stop (Shift+F5)* lopettaa debuggauksen. Huomaa että tätä toimintoa käyttämällä ohjelman suoritus lopetetaan eikä loppua koodia suoriteta.

Kaikki nämä samat toiminnot löytyvät debuggauksen aikana myös VS Coden *Run* valikosta.

Askella eteenpäin (*Step Over (F10)*) kaksi kertaa kunnes number2 muuttuja on alustettu. Huomaa että keltainen *instruction pointer* siirtyy eteenpäin koodissa näyttäen rivin jolla ohjelman suoritus sillä hetkellä on. Nyt näet Locals osassa mikä arvo number2 muuttujassa on. Tässä esimerkissä se muuttujan arvo on 77, mutta sinulla se on luultavasti joku muu arvo koska numero tulee satunnaislukugeneraattorista. Voit tarkastella muuttujien arvoa myös pitämällä hiiren kursoria muuttujan nimen päällä lähdekoodieditorissa.

![](./img/vs-code-debugger-stepping-code.png "VS Code koodin askeltaminen")

Huomaa että voit missä vaiheessa tahansa vaihtaa muuttujien arvoa yksinkertaisesti tuplaklikkaamalla muutujan arvoa *Variables* näkymässä ja syöttämällä muuttujalle halutun arvon.

Pysäytä debuggeri ja lisätään koodi joka vaihtaa number1 ja number2 muuttujien arvot:

    # quite typical task of swapping value between two variables
    # use the debugger to observer how value in variables change while stepping
    # thru the code
    temp = number1
    number1 = number2
    number2 = temp

Askella eteenpäin kunnes olet suorittanut number1-number2 muuttujien arvojen vaihdon. Tarkkaile jokaisella askeleella kuinka debuggerinäkymä päivittää muuttuneet arvot Locals näkymässä ja korostaa edellisen askeleen aikana muuttuneet arvot.


### **Askeltaminen, Step Over toiminto**
Pysäytä debuggeri ja poista kaikki pysäytyskohdat. Tehdään uusi funktio **step**, joka tulostaa hieman tekstiä. Lisää **step** funktion toteutus lähdekooditiedoston alkuun heti **import** rivin jälkeen:

    # use some random numbers to observe in debugger
    from random import *

    def step():
        print("Inside the 'step' function")
        print("All done here, exiting step function")

Tämän jälkeen lisää koodin loppuun **step** funktiokutsu ja lisäksi yksi rivi joka tulostaa tekstiä. Viimeinen **print** rivi on koodissa vain siksi että debuggerilla olisi jotain suoritettavaa **step** funktion jälkeen ja voimme helpommin tutustua *Step Over*, *Step Into* ja *Step Out* toimintojen käyttämiseen.

    # use the debugger to step over the function call
    # then run the debugger again and use the 'Step Into' option instead
    # run the debugger one more time, use 'Step Into', and inside the
    # function, use 'Step Out' option.
    step()

    print("debugger.py, all done, exiting.")

* Lisää uusi pysäytyskohta riville jossa kutsutaan **step** funktiota.
* Käynnistä debuggaus uudelleen (F5 tai *Run->Start Debugging*)
    * Ohjelman suoritus pysähtyy riville jossa kutsutaan **step** nimistä funktiota.
* Askella funktiokutsun yli (F10 tai *Run->Step Over*)
    * Huomaa että alareunan *Terminal* ikkunaan on ilmestynyt **step** funktion tulostama teksti.
    * *Step Over* ei siis tarkoita että yli hypättävä koodi jätetään suorittamatta. Itse asiassa debuggerilla ei voi suoranaisesti jättää koodia suoritamatta.


### **Askeltaminen, Step Into toiminto**
Mikäli ohjelman suoritus on funktiokutsun kohdalla, voit käyttää *Step Into (F11)* toimintoa askeltaa funktion sisään.

* Käynnistä debuggaus uudelleen
    * Ohjelman suoritus pysähtyy riville jossa kutsutaan **step** funktiota.
* Askella funktiokutsun sisään (F11 tai *Run->Step Into*)
    * Askellus hyppää **step** metodin sisään ja voit askeltaa sieltä takaisin ulos *Step Over (F10)* toiminnolla.


### **Askeltaminen, Step Out toiminto**
Jos ohjelman suoritus on funktion sisällä, voit suorittaa funktion loppuun ja pysähtyä funktiokutsua seuraavalle riville käyttäen *Step Out* toimintoa.
* Käynnistä debuggaus uudelleen
    * Ohjelman suoritus pysähtyy riville jossa kutsutaan **step** funktiota.
* Askella funktiokutsun sisään (F11 tai *Run->Step Into*)
* Käytä *Step Out* toimintoa. Debuggeri suorittaa funktion loppuun ja pysähtyy funktiokutsua seuraavalle riville.


## **Funktion argumenttien tarkastelu**
Pysäytä debuggeri ja poista kaikki pysäytyskohdat. Tehdään uusi funktio **step_with_params** ja lisätään lähdekoodiin kutsu siihen.

    def step_with_params(param1, param2):
        # observer the values of the param1 and param2 in debugger
        # step over to see which condition is executed
        if param1 > param2:
            print("param1 is larger than param2")
        else:
            print("param1 is smaller or equal to param2")

        # observe how param1 and param2 values change
        param1 += param2
        param2 += param1

        print("All done here, exiting step_with_params function")
        # use call stack to observe param1 and param2 here and outside where this
        # function was called. Are they same or different? Why?

Lisää uusi pysäytyskohta riville jossa kutsutaan **step_with_params** funktiota. Käynnistä debuggaus ja askella **step_with_params** funktion sisään. Askella **step_with_params** funktiossa ehtolauseiden yli. Tarkastele mihin ehtohaaraan ohjelman suoritus menee. Askella eteenpäin kunnes olet **step_with_params** funktion viimeisellä koodirivillä.

![](./img/vs-code-debugger-arguments.png "Funktion argumenttien tarkastelu VS Code debuggerissa")


 ## **Call Stack näkymän käyttäminen**
Kun olet pysähtynyt **step_with_params** funktion viimeiselle koodiriville, huomaa vasemmalla debugger näkymässä oleva *Call Stack* osa. *Call Stack* näyttää listan kaikista funktioista joita on kutsuttu, mutta ne ei ole vielä palanneet.
* Huomioi param1 ja param2 funktioparametrien arvot, tässä 47 ja 57.
* Avaa *Call Stack* osa ja huomaa että se listaa kaksi funktiota:
    * Alin on nimetty *<module>* koska lähdekoodi ei ole minkään funktion sisällä
    * Ylimpänä listallä näkyy meidän **step_with_params** funktio
    * Voit klikata *Call Stack* listan *<module>* ja debuggeri näyttää ohjelman tilan niinkuin se oli **step_with_params** funktiota kutsuttaessa.
    * Klikkaamalla *Call Stack* listan **step_with_params** funktionimeä pääset takaisin funktion sisään.

Tehtävä: Tarkastele debuggerilla number1 ja number2 muuttujien arvoa silloin kun ne viedään **step_with_params** funktiolle argumentteina. Sen jälkeen tarkastele **step_with_params** funktion param1 ja param2 parametrien arvoja silloin kun menet funktion sisään ja silloin kun niiden arvoja muutetaan **step_with_params** funktion lähdekoodissa. Ovatko number1 ja number2 muutujien arvot eri kuin param1 ja param2? Miksi näin?


## **Debuggeri, muita ominaisuuksia**
*Watch* osioon voit halutessasi lisätä muuttujien nimiä joita haluat seurata debuggerissa. Tämä saattaa olla tarpeen jos VS Code ei jostain syystä pysty näyttämään haluttua muuttujaa *Locals* tai *Globals* osuudessa, tai joskus esimerkiksi *Globals* osuudessa on listattu niin paljon muuttujia että haluttua muuttujaa on vaikea löytää.

Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Debuggeri, yhteenveto**
Debuggeri on erittäin tehokas työkalu ja on vaikea kuvitella että ohjelmoinnissa pääsee kovin pitkälle ilman sitä. Kun teet uuden funktion joka sisältää algoritmin, ota tavaksi askeltaa se läpi debuggerilla. Näin varmistat että algoritmisi todella toimii niinkuin sen ajattelit toimivan.

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

