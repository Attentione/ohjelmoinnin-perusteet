# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Muuttujat ja tietotyypit**
Kielestä ja ympäristöstä riippumatta, ohjelmointi on periaatteessa aina samanlaista:
1. Käytetään tietokoneen muistia tiedon tallentamiseen
2. Kirjoitetaan algoritmeja/funktioita jotka muokkaavat tietoa
3. Kirjoitetaan logiikka jonka perusteella algoritmeja käytetään tiedon muokkaamiseen

Tällä kertaa tutustumme kohtaan 1.

- [Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md): **/examples/07-variables/variables.py**


## **Bitti ja tavu**
Jokainen tietokoneen muistissa oleva numero kuluttaa muistia. Mitä suurempi numero halutaan tallettaa, sitä enemmän muistia pitää käyttää numeron tallentamiseen.

Bitti (bit) on binääriyksikkö joka voi olla vain 0 tai 1. Näin tietokone käytännössä tallettaa tietoa muistiin. Muistipankin yksi osa voi joko johtaa sähköä tai ei. Käytännössä emme useinkaan puhu biteistä muistinkäytön yhteydessä, vaan käytämme perusyksikkö ’tavua’ (byte) joka koostuu 8 bitistä. Koska bitti voi sisältää vain kaksi arvoa (0 ja 1), tavu (8 bittiä) voi sisältää 2 potenssiin 8 eri arvoa.

Tuleeko mieleen asioita jotka mitataan tai esitetään biteissä tavujen sijaan? Miksi näin?

- Kokeile laskea Windowsin laskimella kuinka monta eri arvoa 8 bittiä voi esittää.
    - Vinkki; vaihda laskin 'Ohjelmoija' (Programmer) tilaan.
- Alla kuvassa on 8 bittinen luku, eli tavu. Kokeile laskea laskimella mitä kokonaislukua bitit 01100011 esittävät.
    - ![](./img/bits-in-byte.png "Tavu rakentuu kahdeksasta bitistä")


## **Tietotyypit**
Yleensä ohjelmoidessa ei tarvitse käsitellä bittejä, vaikka se on toki mahdollista. Tietokoneen muistinkäyttöä yleensä käsitellään tavuina, kilotavuina, megatavuina jne.

Nyt tiedämme että 1 tavu (byte) voi esittää 256 eri arvoa (0-255). Entä jos haluamme esittää myös negatiivisia lukuja? Voimme käyttää yhden tavun myös ilmaisemaan etumerkillisiä lukuja (signed) etumerkittömien (unsigned) sijaan. Jos päätämme että 1 tavu on etumerkillinen, siihen voidaan tallettaa samat 256 eri arvoa, mutta arvot tulkitaan välille -128 ja 127.

0-255 ja -128 – 127 ovat melko pieniä lukuja. Onneksi voimme käyttää enemmän tavuja tallentamaan suurempia numeroita.

| tyyppi | Pituus (tavuja) | Negatiiviset luvut (signed) | Arvoalue |
| :-------- | :--------: | :--------: | :--------: |
| kokonaisluku | 1 |   | 0-255 |
| kokonaisluku | 1 | x | -128 - 127 |
| kokonaisluku | 2 |   | 0 - 65535 |
| kokonaisluku | 2 | x | -32768 - 32767 |
| kokonaisluku | 4 |   | 0 - 4294967295 |
| kokonaisluku | 4 | x | -2147483648 - 2147483647 |
| kokonaisluku | 8 |   | 0 - 18446744073709551615 |
| kokonaisluku | 8 | x | -9223372036854775808 - 9223372036854775807 |
| liukuluku | 4 | x | -3.402823e38 - 3.402823e38 |
| liukuluku | 8 | x | -1.79769313486232e308 - 1.79769313486232e308 |
| boolean | 1 |  | true tai false |


## **Muuttujien esittely**
Kun haluat käyttää numeroarvoa ohjelmassasi sinun on esiteltävä sille muuttuja joka tallentaa numeron tietokoneen muistiin. Muuttujat esitellään kirjoittamalla sille ohjelmoijan valitsema nimi ja sijoittamalla siihen jokin arvo.

    number = 5
    accountbalance = 110.54
    print(number)
    print(accountbalance)

Ensimmäinen rivi esittelee 'number' nimisen muuttujan ja alustaa sen arvoon 5. Toinen rivi esittelee liukulukutyyppisen (float) muuttujan ja alustaa sen arvoon 110.54.

Huomaa että Python kielessä ohjelmoijan ei tarvitse erikseen kertoa muuttujan tyyppiä, vaan Python tulkki päättää oikean tietotyypin ohjelman suorituksen aikana.


## **Muuttujien käsittely operaattoreilla**
Muuttujien arvoa voi muuttaa käyttämällä operaattoreita. Yhtä operaattoria olemme jo käyttäneet. **=** on sijoitusoperaattori jolla muuttujaan voi sijoittaa arvon. Arvo voi olla kiinteä, tai toisen muuttujan nimi.

    number = 55
    number2 = number + 2
    # value of number2 is now 57
    print("number2 is ", number2)

Huomaa että yllä olevan esimerkin viimeisellä rivillä numerotietoa sisältävää muuttujaa ***number2*** ei voi suoraan lisätä merkkijonoon + operaattorilla, vaan numero täytyy ensin muuntaa merkkijonoksi ***str*** funktiolla.

Sijoitusoperaattorin lisäksi on muita aritmeettisia operaattoreita joilla voi käsitellä muuttujien arvoja.

### **Matemaattiset operaattorit**
| Operaattori | Tehtävä | Esimerkki |
| :---- | :---- | :---------- |
| + | yhteenlasku | number = 10 + 2   number2 = number + 123 |
| - | vähennyslasku | number = 10 - 2   number2 = number - 123 |
| * | kertolasku | number = 10 * 2   number2 = number * 123 |
| / | jakolasku | number = 10 / 2   number2 = number / 2 |
| % | jakojäännös | number = 10 % 2   number2 = number % 5 |
| ** | potenssiin korotus | number ** 2 |
| // | jako ja alaspäin pyöristys | number // 3 |

### **Sijoitusoperaattorit**
| Operaattori | Tehtävä | Esimerkki |
| :---- | :---- | :---------- |
| = | sijoita arvo | number = 10   number2 = number |
| += | lisää ja sijoita | number += 10 |
| -= | vähennä ja sijoita | number -= 10 |
| *= | kerro ja sijoita | number *= 10 |
| /= | jaa- ja sijoita | number /= 10 |
| %= | jakojäännös ja sijoita | number %= 10 |
| //= | jako ja alaspäin pyöristys + sijoita | number //= 10 |
| **= | potenssiin korotus ja sijoitus | number **= 2 |

Sijoitusoperaattoreissa on myös bittitason operaatioita kuten **AND** ja **OR**, mutta emme käsittele niitä tässä.


## **Muuttujien olemassaolo**
Oletuksena muuttujat varataan muistista niin että ne ovat olemassa vain sen lähdekoodiosan sisällä missä ne on esitelty. Esimerkikisi aliohjelmassa esitellyt muuttujat ja parametrit eivät ole enää käytettävissä kun funktio on suoritettu. Palaamme tähän myöhemmin kun tutustumme tarkemmin funktioiden tekemiseen.


## **Muuttujien tyyppi**
Python päättää muuttujan tietotyypin samalla kun ohjelmaa suoritetaan. Muuttujan tyyppi voidaan myös kysyä koodissa ohjelman ajon aikana.

    # Python assigns an automatic type for each variable
    # the variable type can be checked with type()
    print("Type of the variable 'number' is ", type(number))
    print("Type of the variable 'accountbalance' is ", type(accountbalance))


## **Merkkijonot**
Merkkijono (***string***) on sisäänrakennettu tyyppi joka pitää sisällään tekstiä. Merkkijono osaa muuttaa tarvittavien merkkien määrää automaattisesti ja sisältää useita hyödyllisiä ominaisuuksia. Merkkijonomuuttuja alustetaan lainausmerkeissä **"** olevalla tekstillä. Myös 'hipsu' **'** merkkiä voi käyttää lainausmerkin sijaan.

    # strings
    # initialize string from text in quotes
    name = "Jani"
    lastName = "Immonen"
    
    # initialize string from another string
    fullName = name
    
    # add strings to string
    fullName = name + " " + lastName
    print(fullName)
    
    # use Format function
    age = 26
    fullName = "First name: {}. Last name: {}. Age: {}".format(name, lastName, age)
    print(fullName)


Sisäisesti string koostuu automaattisesti käsitellystä taulukosta merkkejä. Itseasiassa Python kielessä jokainen merkkijonon merkki on merkkijonotyyppiä.

    # access characters in a string
    text = "ABC"
    a = text[0]
    b = text[1]
    c = text[2]
    print("Second char is " + b)

***len*** funktiolla voit kysyä merkkijonon pituuden.

    # print text length into console
    print("Text length is " + str(len(text)) + " characters.")

Merkkijonoja voi verrata ***==*** operaattorilla (Palaamme ehtolauseisiin tarkemmin myöhemmin).

    # compare strings
    text2 = "ABD"
    if text == text2:
        print("Texts are identical.")
    else:
        print("Texts are not identical")

Konsoliohjelmassa voi varsin helposti lukea käyttäjän syöttämän tekstin.

    # read text from console
    line = input("Enter a line of text: ")
    print("You entered " + line)

Voit myös lukea numeroarvon sijoittamalla käyttäjän syöttämän merkkijonon int tyyppisenä.

    # read numeric value to variable 'number'
    # note that if user enters text that cannot be converted to a number, an
    # error is generated. We will later learn how to handle these situations
    line = input("Enter a number: ")
    number = int(line)
    print("Number is ", number)

Pikatehtävä: Kokeile myös muita mielenkiintoisia string funktioita:
- count
- startswith, endswith
- replace
- lower, upper
- split

Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Muuttujat ja tietotyypit, yhteenveto**
Kaikki tietorakenteet yksinkertaisimmasta monimutkaisempaan koostuvat numeroista. Ohjelmoijan tehtävä on jäsennellä numerot järkevään muotoon ja varata tilaa numeroille tietokoneen muistista käyttäen tehtävään sopivia tietotyyppejä.

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

