# **Ohjelmoinnin perusteet**
TTC2030 - 5 op

## **Kokoelmat**
Käytännössä tarvitsemme tietorakenteita joiden avulla voimme varata ja poistaa tietoa dynaamisesti ohjelman ajon aikana.

Python ohjelmointikieli tarjoaa joukon kokoelmaluokkia joiden avulla pystymme varaamaan tietoa dynaamisesti, sekä etsimään sitä tehokkaasti myös suurista tietomääristä.

[Tee uusi esimerkki Git-projektiisi](./uuden-esimerkin-aloittaminen.md)
Lisää lähdekooditiedostot:
**/examples/13-collections/list.py**
**/examples/13-collections/tuple.py**
**/examples/13-collections/set.py**
**/examples/13-collections/dictionary.py**

Python-ohjelmointikielellä on neljä erilaista kokoelmaa:

- ***Lista*** on kokoelma, joka on pysyvässä järjestyksessä ja muokattavissa. Listassa voi olla useita elementtejä (**item**) jotka sisältävät saman tiedon.
- ***Tuple*** on pysyvässä järjestyksessä ja sitä ei voi alustuksen jälkeen muokata. Tuplessa voi olla useita elementtejä jotka sisältävät saman tiedon.
- ***Set*** on kokoelma, jonka järjestys voi muuttua ja se on *indeksoimaton*. Set ei salli useita samansisältöisiä elementtejä.
- ***Dictionary*** on kokoelma, jonka järjestys voi muuttua, on muokattavissa ja *indeksoitu*. Dictionary ei salli useita samansisältöisiä elementtejä.

Kokoelmatyyppiä valittaessa on hyödyllistä ymmärtää kyseisen tyypin ominaisuudet. Tietyn tietojoukon oikean tyypin valitseminen voi tarkoittaa merkityksen säilymistä, ja se voi tarkoittaa tehokkuuden tai turvallisuuden lisäämistä.

## **Lista**
Listan esitellään ja alustetaan käyttämättä hakasulkuoperaattoreita. ***append*** ja ***insert*** funktioilla voit lisätä listaan elementtejä:

    # declare a list of numbers and initialize
    list = [5, 10, 15]

    # append adds new item to the end of the list
    list.append(100)

    # insert new item to the middle of the list
    list.insert(1, 7)

    # print the contents of the list
    print("List contents: ", list)

Käytössä olevan listan elementteihin voidaan viitata hakasulkuoperaattoreiden avulla:

    # list items can be accessed via [] operator
    # note that index in [] is zero based
    print("List element at index 1: ", list[1])

    # index to access list can be negative
    # negative index means beginning from the end of the list
    print("List element at index -1: ", list[-1])

    # list item can be changed with [] operator
    list[1] = 60

    # index can be also specified as a range
    # range parameters are start index (inclusive) and end index (exclusive)
    print("List elements at range 2:5: ", list[2:5])

Listan elementtejä voi poistaa ***remove*** ja ***pop*** funktioden avulla, tai ***del*** operaattorilla. ***clear*** funktio tyhjentää listan:

    # remove item from the list
    # only a first occurrence of the item is removed
    list.remove(10)
    print("List contents after 10 was removed: ", list)

    # pop function allows to remove a specific item
    list.pop(1)
    print("List contents after pop(1): ", list)

    # another way to remove item at index is 'del'
    del list[1]
    print("List contents after del list[1]: ", list)

    # clear removes all items from the list
    list.clear()

Listan elementit voidaan käydä läpi ***for*** silmukalla. ***len*** funktiolla voit tarkastaa listan pituuden:

    # declare list of names and print
    namelist = ["Joe", "Sally", "Liam", "Robert", "Emma", "Isabella"]
    print(namelist)

    # loop thru a list
    for name in namelist:
        print(name)

    # use 'len' function to determine list length
    print("namelist length is: ", len(namelist))

Ehtolausessa voit tarkastaa löytyykö tiettyä elementtiä listasta:

    # check if item exists
    if "Liam" in namelist:
        print("Yes, 'Liam' is in the names list")

Listaa ei voi kopioida sijoittamalla sen suoraan toiseen muuttujaan. Tämä luo **viittauksen** alkuperäiseen listaan jolloin molemmat muuttujat muokkaavat samaa listaa:

    # note that modifying the 'anothernamelist' also modifies the 'namelist'
    # as both variables reference the same list
    anothernamelist = namelist
    anothernamelist[0] = "Roger"
    print(namelist)

Jos listasta tarvitaan uusi kopio, käytä listan ***copy*** funktiota:

    # if you need another copy, use list copy function
    anothernamelist = namelist.copy()
    anothernamelist[0] = "Harris"
    anothernamelist[-1] = "Mary"
    print("Contents of namelist: ", namelist)
    print("Contents of anothernamelist: ", anothernamelist)

Listoja voi yhdistää käyttämällä ***+*** operaattoria. Toinen tapa yhdistää listoja on listan ***extend*** funktio:

    # join list to another
    combinedlist = namelist + anothernamelist
    # another way is to use list 'extend' function
    #combinedlist = namelist.copy()
    #combinedlist.extend(anothernamelist)
    print("Contents of combinedlist: ", combinedlist)


## **Tuple**
Tuple on listatyyppinen kokoelma jonka sisältöä ei voi alustuksen jälkeen muokata. Tuple alustetaan kuten lista, mutta hakasulkeiden sijaan elementtien esittelyssä käytetään sulkuja:

    # declare tuple of names and print
    nametuple = ("Joe", "Sally", "Liam", "Robert", "Emma", "Isabella")
    print("Contents of nametuple is: ", nametuple)

Käytössä olevan tuplen elementteihin voidaan viitata hakasulkuoperaattoreiden avulla.

    # tuple items can be accessed via [] operator
    # note that index in [] is zero based
    print("Tuple element at index 1: ", nametuple[1])

    # index to access tuple can be negative
    # negative index means beginning from the end
    print("Tuple element at index -1: ", nametuple[-1])

    # index can be also specified as a range
    # range parameters are start index (inclusive) and end index (exclusive)
    print("Tuple elements at range 2:5: ", nametuple[2:5])

 Tuplen elementtejä ei voi lisätä tai muokata sen esittelyn jälkeen. Tuple on kuitenkin helppo muuttaa tarvittaessa listaksi muokkausta varten. Muokkauksen jälkeen listasta voidaan tehdä tuple:

    # items of the tuple cannot be modified after its declared, though
    # it is possible to convert tuple to list, modify the list and
    # convert list back to tuple
    namelist = list(nametuple)
    namelist[1] = "Mary"
    nametuple = tuple(namelist)
    print("Contents of nametuple is: ", nametuple)

Tuple voidaan käydä läpi ***for*** silmukassa ja sen sisältöä voidaan verrata ehtolauseessa samalla tavalla kuin listan. Eräs tuplen erikoisuus on että jos haluat esitellä tuplen jossa on vain yksi elementti, sinun täytyy lisätä elementin perään pilkku. Muuten Python ei tunnista muuttujan olevan tuple tyyppinen:

    # tuple with only one item must be declared with trailing comma
    nametuple = ("Joe",)
    print(type(nametuple))

    # note that on below the variable is not a tuple
    nametuple = ("Joe")
    print(type(nametuple))

## **Set**
Set on kokoelma joka järjestetään ajon aikana jotta tiedon hakeminen siitä olisi nopeampaa. Setin jäseniin ei voi viitata hakasulkuoperaattoreilla koska sillä ei ole indeksejä. Setin elementtejä ei myöskään voi muokata, mutta niitä voi lisätä ja poistaa. Set alustetaan kuten lista, mutta hakasulkeiden sijaan elementtien esittelyssä käytetään aaltosulkuja:

    # declare set of names and print
    nameset = {"Joe", "Sally", "Liam", "Robert", "Emma", "Isabella"}
    print("Contents of nameset is: ", nameset)

Huomaa että alustuksen jälkeen setin elementit tulostuu eri järjestyksessä kuin ne on alustuksessa annettu. Hakasulkuja ei voi käyttää yksittäisten elementtien viittaukseen mutta setin voi käydä läpi ***for*** silmukalla ja sen pituuden voi tarkastaa ***len*** funktiolla:

    # print contents of the set in for loop
    for name in nameset:
        print(name)

    # check length of the set
    print("Length of the set is: ", len(nameset))

Voit tarkastaa löytyykö joku tietty elementti setistä:

    # check if certain item is in the set
    name = "Sally"
    print(name + " is in set: ", name in nameset)

Setin elementtejä ei voi muuttaa, mutta niitä voi lisätä ja poistaa. ***remove*** funktiolla voi poistaa elementin. ***remove*** aiheuttaa virheen mikäli poistettavaa elementtiä ei löydy. ***discard*** toimii samoin kun ***remove*** mutta ei aiheuta virhettä mikäli poistettavaa elementtiä ei löydy.

    # items in set cannot be modified but can be added and removed
    # use add function to add new item
    nameset.add("Bella")
    print("Contents of nameset after add is: ", nameset)

    # update function allows to add multiple items to the set
    nameset.update({"Harry", "Elisa", "Pocahontas"})
    print("Contents of nameset after update is: ", nameset)

    # items can be removed with remove or discard function
    # remove function will raise an error if item is not in a set
    # discard function works the same as remove but will not raise an error
    # if item is not found
    nameset.remove("Liam")
    #nameset.discard("Liam")
    print("Contents of nameset after remove is: ", nameset)

## **Dictionary**
Dictionary on avain-arvopari tietorakenne. Jokainen dictionaryn alkio sisältää avaimen ja siihen liitetyn arvon. Avainarvon täytyy olla uniikki dictionaryn sisällä. Puolitusalgoritmin ansiosta dictionarystä etsiminen on paljon nopeampaa verrattuna siihen että käydään läpi taulukkoa alusta alkaen. Haittapuolena on että dictionaryyn uuden alkion lisääminen on raskaampi operaatio kuin aikaisemmin esiteltyihin kokoelmiin.

Dictionary esitellään laittamalla sen elementit aaltosulkuihin niin että kaksoispiste erottaa avaimen ja arvon toisistaan:

    # dictionary is a key-value pair collection
    # declare a dictionary of books as ISBN as key and
    # book name as value
    bookdict = {
        12345678: "Book 1",
        12345679: "Book 2",
        12345680: "Book 3",
        12345681: "Book 4",
        12345682: "Book 5",
    }
    print("Contents of the bookdict is: ", bookdict)

Dictionaryn arvoihin voidaan viitata hakasulkuoperaattoreilla ja avaimella, tai ***get*** metodilla:

    # access dictionary items with brackets and key
    bookname = bookdict[12345680]
    #bookname = bookdict.get(12345680)
    print("Book is: " + bookname)

Dictionaryn avaimet ja arvot (tai molemmat) voidaan käydä läpi ***for*** silmukassa:

    # print all keys in the dictionary
    for isbn in bookdict:
        print(isbn)

    # print all values in the dictionary
    for bookname in bookdict.values():
        print(bookname)

    # print keys and values in the dictionary
    for isbn, bookname in bookdict.items():
        print(isbn, bookname)

Voit tarkastaa löytyykö dictionarystä tietty avain:

    # check if specific key exists in dictionary
    if 12345679 in bookdict:
        print("12345679 found in bookdict")

Voit lisätä avain-arvo pareja viittaamalla hakasulkuoperaattoreilla. ***pop*** funktiolla voi poistaa elementin avaimen perusteella:

    # add item to dictionary
    bookdict[422344] = "New Book"
    print("Contents of the bookdict after adding new item is: ", bookdict)

    # remove item with pop and key
    bookdict.pop(12345678)
    print("Contents of the bookdict after pop is: ", bookdict)


Nyt [vie uudet tiedostot versionhallintaan](./uuden-esimerkin-aloittaminen.md).


## **Kokoelmat, yhteenveto**
Tässä esiteltiin tyypillisimmät kokoelmat. Tärkeintä on että osaa valita oikeantyyppinen kokoelma vallitsevan ongelman mukaan.

&nbsp;
----
**© Jani Immonen ja Esa Salmikangas**

