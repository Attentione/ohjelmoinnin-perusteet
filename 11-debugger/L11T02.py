'''
Tee ohjelma, joka kysyy käyttäjältä ensin muutetaanko fahrenheitit celsiuksiksi vai celsius-asteet fahrenheitiksi. 
Tämän jälkeen kysytään käyttäjältä asteet ja muutetetaan ne toisiin asteisiin ja näytetään tulos käyttäjälle.
'''


yksikko = input("Kirjoita F, jos haluat muuttaa fahrenheitit celsiukseksi tai C, jos haluat muuttaa celsiukset fahrenheiteiksi ")

arvo = float(input("Anna lämpötila numerona: "))

def muunnos(yksikko, arvo):
    if yksikko == "F" or yksikko == "f":
        tulos = ((arvo-32)*(5/9))
        return print(f"Lämpötila on {tulos} celssiusta")
    elif yksikko == "C" or yksikko == "c":
        tulos = ((arvo*(9/5))+32)
        return print(f"Lämpötila on {tulos} fahrenheittia")
    else:
        return print("Anna C tai F")



muunnos(yksikko,arvo)        
        

