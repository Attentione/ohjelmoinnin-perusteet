'''
Kysy käyttäjältä aika sekunteina kokonaislukuna, ja muuta se muotoon tunnit:minuutit:sekunnit
Esimerkiksi käyttäjä antaa luvun 10000, niin
ohjelma näyttää sen muodossa "2:46:40".
Kokeile ohjelmasi toimivuus vähintään viidellä eri arvolla.
'''

sekunnit = int(input("Anna aika sekunteina kokonaislukuna: "))

def aika(sekunnit):
    tunti = (sekunnit//3600)
    minuutti = ((sekunnit%3600)//60)
    sekuntti= ((sekunnit%3600)%60)
    print(f"'{tunti}:{minuutti}:{sekuntti}'")



aika(sekunnit)
aika(10000)
aika(90)
aika(3600)
aika(5)
aika(200000)