'''
Mäkihypyssä käytetään viittä arvostelutuomaria. 
Kirjoita ohjelma, joka kysyy arvostelupisteet yhdelle hypylle ja tulostaa tyylipisteiden summan siten, että summasta on vähennetty pois pienin ja suurin tyylipiste.
'''


def tyylipisteet():
    lista = []
    
    for i in range(5):
        pisteet = int(input("Anna pisteet: "))
        lista.append(pisteet)
    lista.remove(max(lista))
    lista.remove(min(lista))
    total = sum(lista)
        
    return total



print("==>> Total points",tyylipisteet())    

