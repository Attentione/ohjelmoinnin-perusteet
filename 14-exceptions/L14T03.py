'''
Lue ensin seuraava keskustelu: https://stackoverflow.com/questions/878943/why-return-notimplemented-instead-of-raising-notimplementederror
Toteuta uusi funktio ja laita se heittämään NotImplemented.
Toteuta toinen funktio ja laita se heittämään NotImplementedError.
Kokeile kutsua toisesta ohjelmasta funktioita. Mitä eroja havaitset. Vastaukset tehtävän alkuun kommenteina
'''


