'''
Tee kokoelma, jossa on 5 merkkijonoa.
Kysy käyttäjältä indeksi mihin kohtaan taulukkoa käyttäjä haluaa syöttää uuden tekstin.
Kysy käyttäjältä uusi teksti ja laita se taulukkoon käyttäjän antamaan indeksiin.
Tulosta taulukon sisältö.
Korjaa ohjelma niin ettei se kaadu, jos käyttäjä syöttää indeksin, joka on taulukon ulkopuolella.
Kerro käyttäjälle mikäli indeksi ei ole kelvollinen ja pyydä syöttämään se uudestaan.
'''





def listaus():
    teksti = input("Anna lisättävä teksti; ")
    while True:
        lista = ["kissa", "koira" , "tiikeri", "leijona", "possu"]
        try:
            luku = int(input("Mihin taulukon indexiin haluat lisätä tekstiä? "))
            lista[luku] = teksti
            return lista
        except ValueError:
            print("Anna kokonaisluku")
        except IndexError:
            print("Indexiä ei löydy anna uusi indexi: ")
        
print(listaus())

