'''
Tee ohjelma, jolla yrität lukea Windows-koneella kaikki tiedostot kovalevyn C:n juuresta (macOS/Linux/Unix- koneilla yritä lukea käyttäjän juurihakemisto). 
Näytä tiedostot konsolilla. Koeta sen jälkeen lisätä tiedosto 'ayho.txt' C:n juureen (macOS/Linux/Unix -koneilla käyttäjän juurihakemistoon).
Mitä tapahtui? Korjaa ohjelma niin, ettei se kaadu.
'''

import os

kansio = os.listdir("c:/")

try:
    tiedosto = open("c:/ayho.txt", "a")

except PermissionError:
    print("Ei riitä oikeudet")
    

# Sudo oikeuksilla voi kirjoitella macOS/Linux/Unix -koneilla

print(kansio)