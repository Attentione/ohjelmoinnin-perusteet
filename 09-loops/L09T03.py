'''
Tee ohjelma, joka kysyy ensin käyttäjältä jonkin luvun väliltä 1-10.
Tämän jälkeen näytä luvut yhdestä annettuun lukuun ja luvun neliön. Esimerkiksi jos käyttäjä antaa luvun 4, niin tuloste olisi seuraavanlainen:
luvun 1 neliö on 1
luvun 2 neliö on 4
luvun 3 neliö on 9
luvun 4 neliö on 16
'''


luku = int(input("Anna kokonaisluku väliltä 1-10: "))



for i in range(1, luku+1):
    tulos = i ** 2 
    print(f"Luvun {i} neliö on {tulos}")
    
