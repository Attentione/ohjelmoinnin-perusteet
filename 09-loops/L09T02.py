'''
Tee ohjelma, joka kysyy käyttäjältä kokonaislukuja. Lukuja kysytään siihen saakka kunnes käyttäjä antaa tyhjän syötteen. 
Laske kuinka monta lukua käyttäjä antoi, laske myös annettujen lukujen summa. Näytä annettujen lukujen lukumäärä ja summa käyttäjälle.
'''



summa = 0
luku = 0

while True:
    muuttuja = (input("Anna kokonais luku: Lopettaaksesi anna TYHJÄ SYÖTE "))
    if  muuttuja == "":
        break
    else:
        muuttuja = int(muuttuja)
        summa += muuttuja
        luku += 1


print(f"Annoit {luku} lukua")
print("Lukujen summa = ", summa)