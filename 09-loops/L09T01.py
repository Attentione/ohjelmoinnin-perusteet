'''
Tee ohjelma, joka kysyy käyttäjältä viikon kunkin päivän sademäärän. 
Sademäärä annetaan kokonaislukuna, jollei kyseisenä päivänä ole satanut käyttäjä antaa luvuksi 0. Laske ja näytä viikon kokonaissademäärä.
'''
summa = 0
luku = 0

while luku <= 6:
    sade = int(input("Anna päivän sademäärä: "))
    summa += sade
    luku += 1


print(f"Seitsemän päivän sademäärä oli {summa}mm")
