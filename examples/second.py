'''
Uusi kansio ja Python-tiedosto versionhallintaan.

Tee paikalliseen repoosi/projektiisi uusi kansio nimeltä "examples"
Tee Visual Studio Codella uusi Python-tiedosto  "second.py" edellä luotuun kansioon
Muokkaa Visual Studio Codella tiedostoa, että konsoliin tulostuu oma nimesi ja teksti " my second python program rules!"
Lisää ja kommitoi muutos gitiin
Pushaa muutokset, siis hakemisto ja tiedosto GitLabiin
Pushaa muutokset GitLabiin
Tarkista selaimella että kansio tiedostoinen näkyy labranet gitlabissa
'''

print("Atte Alpia\nmy second python program rules!")