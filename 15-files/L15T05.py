'''
Tee ohjelma, joka arpoo lottorivin ja tallentaa ne tekstitiedostoon 'lotto.txt'.
Arvottu rivi sisältää seitsemän (7) numeroa väliltä 1-40. Varmista arpoessasi riviä että sama numero ei voi esiintyä kahta kertaa.
'''

import random

#random.randrange(1, 40))



setti = set()


while True:

    luku = random.randrange(1, 40)
    
    if len(setti) < 7:
        setti.add(luku)

    else:
        print(setti)
        with open("lotto.txt", "w") as filu:
            for i in setti:
                filu.write(str(i) + " ")
        break