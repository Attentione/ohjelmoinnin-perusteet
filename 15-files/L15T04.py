'''
Tee ohjelma joka kysyy käyttäjältä lukuja (joko kokonaisluku tai liukuluku) ja tallenna kokonaisluvut eri tiedostoon kuin liukuluvut.
Sovellus tulee lopettaa jos käyttäjä ei syötä kokonais- tai liukulukua.
Tarkista tiedostojen sisältö tekstieditorilla.
'''



while True:
    
    try:
        luku = float(input("Lopeta ohjelma antamalla jokin muu syöte kuin luku. Anna kokonais- tai liukuluku: "))
    
       
        if luku %1 == 0:
            with open("15-files/kokonaisluvut.txt", "a") as filu_int:
                filu_int.write(str(luku)+ "\n")
     
        elif luku %1 != 0:
            with open("15-files/liukuluvut.txt","a") as filu_float:
                filu_float.write(str(luku)+ "\n")

    except ValueError:
        break



