'''
Tee ohjelma, joka kysyy käyttäjältä henköiden sukunimiä ja kirjoita käyttäjän antamat nimet tiedostoon (lopetusehdon voit päättää itse).
Avaa tämän jälkeen tiedosto lukemista varten ja tulosta konsoliin tiedoston sisältö riveittäin.
Huomioi mahdolliset poikkeukset, joita tiedoston käsittely voi aiheuttaa.
'''
filunimi = "15-files/sukunimet.txt"


tiedosto = open(filunimi,"w")
while True:
    nimi = input("Tyhjä syöte lopettaa ohjelman. Anna sukunimi: ")
    if nimi != "":
        tiedosto.write(nimi + "\n")
            
    else:
        tiedosto.close()
        break
                


tiedosto = open(filunimi,"r")
lista = tiedosto.read()
tiedosto.close()
print(lista)







