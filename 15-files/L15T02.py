'''
Luo jollakin editorilla (esim Notepadilla) tekstitiedosto 'nimet.txt', johon lisäät vähintään kymmenen naisten ja kymmenen miesten etunimeä.
Tee ohjelma, joka lukee em. tekstitiedostosta nimet ja kertoo montako nimeä löytyy ja montako kertaa kukin nimi esiintyy.
Huomioi myös muut mahdolliset poikkeukset joita tiedoston käsittely voi aiheuttaa.
'''

filunimi = "15-files/nimet.txt"

with open(filunimi, "r") as filu:
    lista_ruma = filu.readlines()

lista = []

for element in lista_ruma:
    lista.append(element.strip())


for x in set(lista):
    print(x+ " #" +str(lista.count(x)))

print(f"Listassa on {len(lista)} nimeä")