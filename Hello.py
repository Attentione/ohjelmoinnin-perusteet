'''
Uusi Python-tiedosto versionhallintaan.

Tee Visual Studio Codella uusi Python-tiedosto "Hello.py" ja lisää se GitLab projektiin
Pushaa tiedostot GitLabiin
Muokkaa Visual Studio Codella niin, että konsoliin tulostuu oma nimesi ja kuluva päivämäärä "Hello World!"
Pushaa muutokset GitLabiin
'''

import datetime


pvm =datetime.datetime.now()

print("Hello world\nAtte Alpia\n",pvm)
