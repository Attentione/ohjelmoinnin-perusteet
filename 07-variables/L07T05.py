
'''
Esittele muuttuja johon tallennetaan pankkitilin saldo euroina.
Kysy konsolissa kuinka monta euroa lisätään saldoon.
Kysy konsolissa kuinka monta senttiä lisätään saldoon.
Tulosta saldo konsoliin näiden muutosten jälkeen.
'''


saldo = float(input("Anna pankkitilin saldo? "))
eurot = int(input("Montako kokonaista euroa lisätään pankkitilille? "))
sentit = float(input("Montako senttiä lisätään pankkitilille? "))
muunnos = sentit/100

total = eurot + muunnos + saldo




print("Pankkitilin saldo =", total)
