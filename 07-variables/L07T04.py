
'''
Tee ohjelma joka kysyy käyttäjältä kaksi kokonaislukua ja tulostaa niiden:

Summan
Erotuksen
Tulon
Jakolaskun tuloksen
'''

luku1 = int(input("anna kokonaisluku: "))
luku2 = int(input("anna kokonaisluku: "))


print("summa =", (luku1+luku2))
print("erotus =", (luku1-luku2))
print("kertolaskun tulos =", (luku1*luku2))
print("jakolaskun tulos =", (luku1/luku2))
